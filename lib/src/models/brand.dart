import 'package:ecommerce_app_ui_kit/src/models/product.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Brand {
  String id;
  String name;
  String logo;
  bool selected;
  List<Product> products;
  Color color;

  Brand(this.id, this.name, this.logo, this.color, this.selected, this.products);
}

class BrandsList {
  List<Brand> _list;

  List<Brand> get list => _list;

  BrandsList() {
    _list = [
      new Brand('1','Wilson', 'img/logo-03.svg', Colors.greenAccent, true,  [
        // products
      ]),
    ];
  }

  selectById(String id) {
    this._list.forEach((Brand brand) {
      brand.selected = false;
      if (brand.id == id) {
        brand.selected = true;
      }
    });
  }
}
