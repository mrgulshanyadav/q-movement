import 'package:ecommerce_app_ui_kit/src/models/product.dart';
import 'package:flutter/material.dart';

class Categry {
  String id;
  String name;
  bool selected;
  String icon;
  List<Product> products;

  Categry(this.id, this.name, this.icon, this.selected, this.products);
}

class SubCategory {
  String id = UniqueKey().toString();
  String name;
  bool selected;
  List<Product> products;

  SubCategory(this.name, this.selected, this.products);
}

class CategoriesList {
  List<Categry> _list;

  List<Categry> get list => _list;

  CategoriesList() {
    this._list = [
      new Categry('1','Man', 'http://hajsjhash', true, [
        // products
      ]),
    ];
  }

  selectById(String id) {
    this._list.forEach((Categry category) {
      category.selected = false;
      if (category.id == id) {
        category.selected = true;
      }
    });
  }

  void clearSelection() {
    _list.forEach((category) {
      category.selected = false;
    });
  }
}

class SubCategoriesList {
  List<SubCategory> _list;

  List<SubCategory> get list => _list;

  SubCategoriesList() {
    this._list = [
      new SubCategory('Sh', true, [
        // products
      ]),
      new SubCategory('Sweater', false, [
        // products
      ]),
      new SubCategory('Jacket', false, [
        // products
      ]),
    ];
  }

  selectById(String id) {
    this._list.forEach((SubCategory subCategory) {
      subCategory.selected = false;
      if (subCategory.id == id) {
        subCategory.selected = true;
      }
    });
  }

  void clearSelection() {
    _list.forEach((category) {
      category.selected = false;
    });
  }
}
