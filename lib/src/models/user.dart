import 'package:intl/intl.dart' show DateFormat;

enum UserState { available, away, busy }

class User {
  String id;
  String name;
  String email;
  String phone;
  String gender;
  DateTime dateOfBirth;
  String avatar;
  String address;
  UserState userState;

  User.init();

  User.basic(this.name, this.avatar, this.userState);

  User.advanced(this.id, this.name, this.email, this.phone, this.gender, this.dateOfBirth, this.avatar, this.address, this.userState);

  User getCurrentUser() {
    return User.advanced(id,name,email, phone , gender, dateOfBirth, avatar, address, userState);
  }

  getDateOfBirth() {
    return DateFormat('yyyy-MM-dd').format(this.dateOfBirth);
  }


}
