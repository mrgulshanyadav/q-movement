
class Product {
  String id;
  String title;
  String slug;
  String category;
  String subcategory;
  String isOriginalRceipt;
  String isUnderWarranty;
  String isWithCase;
  String condition;
  String oldYear;
  String oldMonth;
  String whatsappNumber;
  String contactNumber;
  String fixedNegotiable;
  String color;
  String type;
  String material;
  String newOld;
  String modelName;
  String brandName;
  String brand;
  String description;
  String price;
  String negotiable;
  String city;
  String country;
  String state;
  String location;
  String lang;
  String lat;
  String postalcode;
  String tags;
  String img1;
  String img2;
  String img3;
  String img4;
  String boxAndPaperImage;
  String isOriginalRciept;
  String seller;
  String package;
  String adminView;
  String howmuchtimeremaining;
  dynamic fixedNegitiable;
  String isFeatured;
  String isStatus;
  String deleteRequest;
  String updatedDate;
  String createdDate;
  String expiryDate;


  Product(this.id, this.title, this.slug, this.category, this.subcategory,
      this.isOriginalRceipt, this.isUnderWarranty, this.isWithCase,
      this.condition, this.oldYear, this.oldMonth, this.whatsappNumber,
      this.contactNumber, this.fixedNegotiable, this.color, this.type,
      this.material, this.newOld, this.modelName, this.brandName, this.brand,
      this.description, this.price, this.negotiable, this.city, this.country,
      this.state, this.location, this.lang, this.lat, this.postalcode,
      this.tags, this.img1, this.img2, this.img3, this.img4,
      this.boxAndPaperImage, this.isOriginalRciept, this.seller, this.package,
      this.adminView, this.howmuchtimeremaining, this.fixedNegitiable,
      this.isFeatured, this.isStatus, this.deleteRequest, this.updatedDate,
      this.createdDate, this.expiryDate);

  String getPrice({double myPrice}) {
    if (myPrice != null) {
      return 'QR '+'${myPrice.toStringAsFixed(2)}';
    }
    return 'QR '+'${this.price}';
  }
}

class ProductsList {
  List<Product> _flashSalesList;
  List<Product> _list;
  List<Product> _categorized;
  List<Product> _favoritesList;
  List<Product> _cartList;

  set categorized(List<Product> value) {
    _categorized = value;
  }

  List<Product> get categorized => _categorized;

  List<Product> get list => _list;
  List<Product> get flashSalesList => _flashSalesList;
  List<Product> get favoritesList => _favoritesList;
  List<Product> get cartList => _cartList;

  ProductsList() {
    _flashSalesList = [
    ];

    _list = [
    ];

    _favoritesList = [
    ];

    _cartList = [
    ];
  }
}
