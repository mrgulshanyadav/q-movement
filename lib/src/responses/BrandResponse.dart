class BrandResponse {
  String status;
  List<Categories> categories;

  BrandResponse({this.status, this.categories});

  BrandResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['categories'] != null) {
      categories = new List<Categories>();
      json['categories'].forEach((v) {
        categories.add(new Categories.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.categories != null) {
      data['categories'] = this.categories.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Categories {
  String id;
  String nameEng;
  String nameArb;
  String image;
  String isActive;
  String createdAt;

  Categories(
      {this.id,
        this.nameEng,
        this.nameArb,
        this.image,
        this.isActive,
        this.createdAt});

  Categories.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nameEng = json['name_eng'];
    nameArb = json['name_arb'];
    image = json['image'];
    isActive = json['is_active'];
    createdAt = json['created_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name_eng'] = this.nameEng;
    data['name_arb'] = this.nameArb;
    data['image'] = this.image;
    data['is_active'] = this.isActive;
    data['created_at'] = this.createdAt;
    return data;
  }
}
