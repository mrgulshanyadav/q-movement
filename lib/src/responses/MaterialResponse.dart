class MaterialResponse {
  bool status;
  List<Condition> condition;

  MaterialResponse({this.status, this.condition});

  MaterialResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['condition'] != null) {
      condition = new List<Condition>();
      json['condition'].forEach((v) {
        condition.add(new Condition.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.condition != null) {
      data['condition'] = this.condition.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Condition {
  String id;
  String sortname;
  String name;
  String nameArb;
  String slug;
  String status;

  Condition(
      {this.id,
        this.sortname,
        this.name,
        this.nameArb,
        this.slug,
        this.status});

  Condition.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    sortname = json['sortname'];
    name = json['name'];
    nameArb = json['name_arb'];
    slug = json['slug'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['sortname'] = this.sortname;
    data['name'] = this.name;
    data['name_arb'] = this.nameArb;
    data['slug'] = this.slug;
    data['status'] = this.status;
    return data;
  }
}
