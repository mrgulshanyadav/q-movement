class SignInResponse {
  bool status;
  UserDetails userDetails;

  SignInResponse({this.status, this.userDetails});

  SignInResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    userDetails = json['userDetails'] != null
        ? new UserDetails.fromJson(json['userDetails'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.userDetails != null) {
      data['userDetails'] = this.userDetails.toJson();
    }
    return data;
  }
}

class UserDetails {
  String id;
  String username;
  String firstname;
  String lastname;
  String email;
  String contact;
  String password;
  String gender;
  String country;
  String state;
  String city;
  String address;
  String profilePicture;
  String birthdate;
  String profileCompleted;
  String isActive;
  String isVerify;
  String isAdmin;
  String token;
  String passwordResetCode;
  String lastIp;
  String adminView;
  String updatedDate;
  String createdDate;

  UserDetails(
      {this.id,
        this.username,
        this.firstname,
        this.lastname,
        this.email,
        this.contact,
        this.password,
        this.gender,
        this.country,
        this.state,
        this.city,
        this.address,
        this.profilePicture,
        this.birthdate,
        this.profileCompleted,
        this.isActive,
        this.isVerify,
        this.isAdmin,
        this.token,
        this.passwordResetCode,
        this.lastIp,
        this.adminView,
        this.updatedDate,
        this.createdDate});

  UserDetails.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    username = json['username'];
    firstname = json['firstname'];
    lastname = json['lastname'];
    email = json['email'];
    contact = json['contact'];
    password = json['password'];
    gender = json['gender'];
    country = json['country'];
    state = json['state'];
    city = json['city'];
    address = json['address'];
    profilePicture = json['profile_picture'];
    birthdate = json['birthdate'];
    profileCompleted = json['profile_completed'];
    isActive = json['is_active'];
    isVerify = json['is_verify'];
    isAdmin = json['is_admin'];
    token = json['token'];
    passwordResetCode = json['password_reset_code'];
    lastIp = json['last_ip'];
    adminView = json['admin_view'];
    updatedDate = json['updated_date'];
    createdDate = json['created_date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['username'] = this.username;
    data['firstname'] = this.firstname;
    data['lastname'] = this.lastname;
    data['email'] = this.email;
    data['contact'] = this.contact;
    data['password'] = this.password;
    data['gender'] = this.gender;
    data['country'] = this.country;
    data['state'] = this.state;
    data['city'] = this.city;
    data['address'] = this.address;
    data['profile_picture'] = this.profilePicture;
    data['birthdate'] = this.birthdate;
    data['profile_completed'] = this.profileCompleted;
    data['is_active'] = this.isActive;
    data['is_verify'] = this.isVerify;
    data['is_admin'] = this.isAdmin;
    data['token'] = this.token;
    data['password_reset_code'] = this.passwordResetCode;
    data['last_ip'] = this.lastIp;
    data['admin_view'] = this.adminView;
    data['updated_date'] = this.updatedDate;
    data['created_date'] = this.createdDate;
    return data;
  }
}
