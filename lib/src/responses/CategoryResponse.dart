class CategoryResponse {
  String status;
  List<Categories> categories;

  CategoryResponse({this.status, this.categories});

  CategoryResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['categories'] != null) {
      categories = new List<Categories>();
      json['categories'].forEach((v) {
        categories.add(new Categories.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.categories != null) {
      data['categories'] = this.categories.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Categories {
  String id;
  String name;
  String slug;
  String description;
  String picture;
  String status;
  String topCategory;
  String showOnHome;
  String createdAt;

  Categories(
      {this.id,
        this.name,
        this.slug,
        this.description,
        this.picture,
        this.status,
        this.topCategory,
        this.showOnHome,
        this.createdAt});

  Categories.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    slug = json['slug'];
    description = json['description'];
    picture = json['picture'];
    status = json['status'];
    topCategory = json['top_category'];
    showOnHome = json['show_on_home'];
    createdAt = json['created_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['slug'] = this.slug;
    data['description'] = this.description;
    data['picture'] = this.picture;
    data['status'] = this.status;
    data['top_category'] = this.topCategory;
    data['show_on_home'] = this.showOnHome;
    data['created_at'] = this.createdAt;
    return data;
  }
}
