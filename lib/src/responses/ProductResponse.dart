// To parse this JSON data, do
//
//     final productResponse = productResponseFromJson(jsonString);

import 'dart:convert';

ProductResponse productResponseFromJson(String str) => ProductResponse.fromJson(json.decode(str));

String productResponseToJson(ProductResponse data) => json.encode(data.toJson());

class ProductResponse {
  String status;
  int count;
  List<Category> categories;

  ProductResponse({
    this.status,
    this.count,
    this.categories,
  });

  factory ProductResponse.fromJson(Map<String, dynamic> json) => ProductResponse(
    status: json["status"],
    count: json["count"],
    categories: List<Category>.from(json["categories"].map((x) => Category.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "count": count,
    "categories": List<dynamic>.from(categories.map((x) => x.toJson())),
  };
}

class Category {
  String id;
  String title;
  String slug;
  String category;
  String subcategory;
  String isOriginalRceipt;
  String isUnderWarranty;
  String isWithCase;
  String condition;
  String oldYear;
  String oldMonth;
  String whatsappNumber;
  String contactNumber;
  String fixedNegotiable;
  String color;
  String type;
  String material;
  String newOld;
  String modelName;
  String brandName;
  String brand;
  String description;
  String price;
  String negotiable;
  String city;
  String country;
  String state;
  String location;
  String lang;
  String lat;
  String postalcode;
  String tags;
  String img1;
  String img2;
  String img3;
  String img4;
  String boxAndPaperImage;
  String isOriginalRciept;
  String seller;
  String package;
  String adminView;
  String howmuchtimeremaining;
  dynamic fixedNegitiable;
  String isFeatured;
  String isStatus;
  String deleteRequest;
  String updatedDate;
  String createdDate;
  String expiryDate;

  Category({
    this.id,
    this.title,
    this.slug,
    this.category,
    this.subcategory,
    this.isOriginalRceipt,
    this.isUnderWarranty,
    this.isWithCase,
    this.condition,
    this.oldYear,
    this.oldMonth,
    this.whatsappNumber,
    this.contactNumber,
    this.fixedNegotiable,
    this.color,
    this.type,
    this.material,
    this.newOld,
    this.modelName,
    this.brandName,
    this.brand,
    this.description,
    this.price,
    this.negotiable,
    this.city,
    this.country,
    this.state,
    this.location,
    this.lang,
    this.lat,
    this.postalcode,
    this.tags,
    this.img1,
    this.img2,
    this.img3,
    this.img4,
    this.boxAndPaperImage,
    this.isOriginalRciept,
    this.seller,
    this.package,
    this.adminView,
    this.howmuchtimeremaining,
    this.fixedNegitiable,
    this.isFeatured,
    this.isStatus,
    this.deleteRequest,
    this.updatedDate,
    this.createdDate,
    this.expiryDate,
  });

  factory Category.fromJson(Map<String, dynamic> json) => Category(
    id: json["id"],
    title: json["title"],
    slug: json["slug"],
    category: json["category"],
    subcategory: json["subcategory"],
    isOriginalRceipt: json["isOriginalRceipt"],
    isUnderWarranty: json["isUnderWarranty"],
    isWithCase: json["isWithCase"],
    condition: json["condition"],
    oldYear: json["old_year"],
    oldMonth: json["old_month"],
    whatsappNumber: json["whatsapp_number"],
    contactNumber: json["contact_number"],
    fixedNegotiable: json["fixed_negotiable"],
    color: json["color"],
    type: json["type"],
    material: json["material"],
    newOld: json["new_old"],
    modelName: json["model_name"],
    brandName: json["brand_name"],
    brand: json["brand"],
    description: json["description"],
    price: json["price"],
    negotiable: json["negotiable"],
    city: json["city"],
    country: json["country"],
    state: json["state"],
    location: json["location"],
    lang: json["lang"],
    lat: json["lat"],
    postalcode: json["postalcode"],
    tags: json["tags"],
    img1: json["img_1"],
    img2: json["img_2"],
    img3: json["img_3"],
    img4: json["img_4"],
    boxAndPaperImage: json["boxAndPaperImage"],
    isOriginalRciept: json["isOriginalRciept"],
    seller: json["seller"],
    package: json["package"],
    adminView: json["admin_view"],
    howmuchtimeremaining: json["howmuchtimeremaining"],
    fixedNegitiable: json["fixed_negitiable"],
    isFeatured: json["is_featured"],
    isStatus: json["is_status"],
    deleteRequest: json["delete_request"],
    updatedDate: json["updated_date"],
    createdDate: json["created_date"],
    expiryDate: json["expiry_date"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "title": title,
    "slug": slug,
    "category": category,
    "subcategory": subcategory,
    "isOriginalRceipt": isOriginalRceipt,
    "isUnderWarranty": isUnderWarranty,
    "isWithCase": isWithCase,
    "condition": condition,
    "old_year": oldYear,
    "old_month": oldMonth,
    "whatsapp_number": whatsappNumber,
    "contact_number": contactNumber,
    "fixed_negotiable": fixedNegotiable,
    "color": color,
    "type": type,
    "material": material,
    "new_old": newOld,
    "model_name": modelName,
    "brand_name": brandName,
    "brand": brand,
    "description": description,
    "price": price,
    "negotiable": negotiable,
    "city": city,
    "country": country,
    "state": state,
    "location": location,
    "lang": lang,
    "lat": lat,
    "postalcode": postalcode,
    "tags": tags,
    "img_1": img1,
    "img_2": img2,
    "img_3": img3,
    "img_4": img4,
    "boxAndPaperImage": boxAndPaperImage,
    "isOriginalRciept": isOriginalRciept,
    "seller": seller,
    "package": package,
    "admin_view": adminView,
    "howmuchtimeremaining": howmuchtimeremaining,
    "fixed_negitiable": fixedNegitiable,
    "is_featured": isFeatured,
    "is_status": isStatus,
    "delete_request": deleteRequest,
    "updated_date": updatedDate,
    "created_date": createdDate,
    "expiry_date": expiryDate,
  };
}
