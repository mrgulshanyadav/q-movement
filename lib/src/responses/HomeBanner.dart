class HomeBanner {
  bool status;
  String message;
  int id;
  List<String> banner;

  HomeBanner({this.status, this.message, this.id, this.banner});

  HomeBanner.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    id = json['id'];
    banner = json['Banner'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    data['id'] = this.id;
    data['Banner'] = this.banner;
    return data;
  }
}
