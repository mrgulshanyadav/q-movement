import 'package:ecommerce_app_ui_kit/config/ui_icons.dart';
import 'package:ecommerce_app_ui_kit/src/models/product.dart';
import 'package:ecommerce_app_ui_kit/src/models/product_color.dart';
import 'package:ecommerce_app_ui_kit/src/models/product_size.dart';
import 'package:ecommerce_app_ui_kit/src/widgets/FlashSalesCarouselWidget.dart';
import 'package:flutter/material.dart';

class ProductHomeTabWidget extends StatefulWidget {
  Product product;
  ProductsList _productsList = new ProductsList();

  ProductHomeTabWidget({this.product});

  @override
  productHomeTabWidgetState createState() => productHomeTabWidgetState();
}

class productHomeTabWidgetState extends State<ProductHomeTabWidget> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                child: Text(
                  widget.product.title,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: Theme.of(context).textTheme.display1,
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 6),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(widget.product.getPrice(), style: Theme.of(context).textTheme.display3.copyWith(fontSize: 20)),
              SizedBox(width: 10),
//              Text(
//                widget.product.getPrice(myPrice: widget.product.price + 10.0),
//                style: Theme.of(context)
//                    .textTheme
//                    .headline
//                    .merge(TextStyle(color: Theme.of(context).focusColor, decoration: TextDecoration.lineThrough)),
//              ),
//              SizedBox(width: 10),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 10, left: 20, right: 20),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                  child: RichText(
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    text: TextSpan(
                        children: [
                          TextSpan(
                            style: Theme.of(context).textTheme.body2,
                            text:  'Brand: ',
                          ),
                          TextSpan(
                            style: Theme.of(context).textTheme.body1,
                            text:  widget.product.brandName,
                          ),
                        ]
                    ),

                  )
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 10, left: 20, right: 20),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                  child: RichText(
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    text: TextSpan(
                        children: [
                          TextSpan(
                            style: Theme.of(context).textTheme.body2,
                            text:  'Model: ',
                          ),
                          TextSpan(
                            style: Theme.of(context).textTheme.body1,
                            text:  widget.product.modelName,
                          ),
                        ]
                    ),



                  )
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 10, left: 20, right: 20),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                  child: RichText(
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    text: TextSpan(
                        children: [
                          TextSpan(
                            style: Theme.of(context).textTheme.body2,
                            text:  'Case: ',
                          ),
                          TextSpan(
                            style: Theme.of(context).textTheme.body1,
                            text:  widget.product.material,
                          ),
                        ]
                    ),



                  )
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 10, left: 20, right: 20),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                  child: RichText(
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    text: TextSpan(
                        children: [
                          TextSpan(
                            style: Theme.of(context).textTheme.body2,
                            text:  'Year: ',
                          ),
                          TextSpan(
                            style: Theme.of(context).textTheme.body1,
                            text:  widget.product.oldYear,
                          ),
                        ]
                    ),



                  )
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 10, left: 20, right: 20),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                  child: RichText(
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    text: TextSpan(
                        children: [
                          TextSpan(
                            style: Theme.of(context).textTheme.body2,
                            text:  'Condition: ',
                          ),
                          TextSpan(
                            style: Theme.of(context).textTheme.body1,
                            text:  widget.product.condition,
                          ),
                        ]
                    ),
                  )
              ),
            ],
          ),
        ),

      ],
    );
  }
}