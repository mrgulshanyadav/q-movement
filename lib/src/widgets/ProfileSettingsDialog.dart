import 'dart:convert';
import 'dart:io';

import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:dio/dio.dart';
import 'package:ecommerce_app_ui_kit/config/ui_icons.dart';
import 'package:ecommerce_app_ui_kit/src/models/user.dart';
import 'package:ecommerce_app_ui_kit/src/responses/SignUpResponse.dart';
import 'package:ecommerce_app_ui_kit/src/responses/UpdateProfileResponse.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart' show DateFormat;
import 'package:shared_preferences/shared_preferences.dart';

class ProfileSettingsDialog extends StatefulWidget {

  @override
  _ProfileSettingsDialogState createState() => _ProfileSettingsDialogState();
}

class _ProfileSettingsDialogState extends State<ProfileSettingsDialog> {
  String userid, name, email, phone, gender, dateofbirth, avtar, address;

  GlobalKey<ScaffoldState> _globalKey = GlobalKey();

  static var uri = "http://qmovement.online/webservices/";

  static BaseOptions options = BaseOptions(
      baseUrl: uri,
      responseType: ResponseType.plain,
      connectTimeout: 30000,
      receiveTimeout: 30000,
      validateStatus: (code) {
        if (code >= 200) {
          return true;
        }
        else{
          return false;
        }
      });

  static Dio dio = Dio(options);

  getUserDetails() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      userid = prefs.getString('currentUserId');
      name = prefs.getString('name');
      email = prefs.getString('email');
      phone = prefs.getString('phone');
      gender = prefs.getString('gender');
      dateofbirth = prefs.getString('dateofbirth');
      avtar = prefs.getString('avtar');
      address = prefs.getString('address');
    });
    print('dateofbirth:'+dateofbirth);
    return {'status':true};
  }

  @override
  void initState() {

    getUserDetails();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      onPressed: () {
        showDialog(
            context: context,
            builder: (context) {
              return SimpleDialog(
                contentPadding: EdgeInsets.symmetric(horizontal: 20),
                titlePadding: EdgeInsets.symmetric(horizontal: 15, vertical: 20),
                title: Row(
                  children: <Widget>[
                    Icon(UiIcons.user_1),
                    SizedBox(width: 10),
                    Text(
                      'Profile Settings',
                      style: Theme.of(context).textTheme.body2,
                    )
                  ],
                ),
                children: <Widget>[
                  Form(
                    child: Column(
                      children: <Widget>[
                        new TextFormField(
                          style: TextStyle(color: Theme.of(context).hintColor),
                          keyboardType: TextInputType.text,
                          decoration: getInputDecoration(hintText: 'John Doe', labelText: 'Full Name'),
                          initialValue: name,
                          validator: (input) => input.trim().length < 3 ? 'Not a valid full name' : null,
                          onSaved: (input) => name = input,
                        ),
                        new TextFormField(
                          style: TextStyle(color: Theme.of(context).hintColor),
                          keyboardType: TextInputType.emailAddress,
                          decoration: getInputDecoration(hintText: 'johndo@gmail.com', labelText: 'Email Address'),
                          initialValue: email,
                          validator: (input) => !input.contains('@') ? 'Not a valid email' : null,
                          onSaved: (input) => email = input,
                        ),
                        new FormField<String>(
                          builder: (FormFieldState<String> state) {
                            return DropdownButtonHideUnderline(
                                child: DropdownButton<String>(
                                  value: gender,
                                  hint: Text("Gender"),
                                  icon: Icon(Icons.keyboard_arrow_down),
                                  iconSize: 24,
                                  onChanged: (String newValue) {
                                    setState(() {
                                      gender = newValue;
                                    });
                                  },
                                  items: ["Male","Female"].map<DropdownMenuItem<String>>((String value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(value),
                                    );
                                  }).toList(),

                                )
                            );
                          },
                        ),
                        new FormField<String>(
                          builder: (FormFieldState<String> state) {

                            String date;
                            if(dateofbirth!=null) {
                              if (dateofbirth.contains(" ")) {
                                date = dateofbirth.split(" ")[0];
                              } else {
                                date = dateofbirth;
                              }
                            }else{
                              date = dateofbirth;
                            }

                            return DateTimeField(
                              decoration: getInputDecoration(hintText: '1996-12-31', labelText: 'Birth Date'),
                              format: new DateFormat('yyyy-MM-dd'),
                              initialValue: DateTime(int.parse(date.split("-")[0]),int.parse(date.split("-")[1]),int.parse(date.split("-")[2])),
                              onShowPicker: (context, currentValue) {
                                return showDatePicker(
                                    context: context,
                                    firstDate: DateTime(1900),
                                    initialDate: currentValue ?? DateTime.now(),
                                    lastDate: DateTime(2100));
                              },
                              onSaved: (input) => setState(() {
                                dateofbirth = DateFormat('yyyy-MM-dd').format(input);
                                // update profile
                              }),
                            );
                          },
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 20),
                  Row(
                    children: <Widget>[
                      MaterialButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: Text('Cancel'),
                      ),
                      MaterialButton(
                        onPressed: _submit(),
                        child: Text(
                          'Save',
                          style: TextStyle(color: Theme.of(context).accentColor),
                        ),
                      ),
                    ],
                    mainAxisAlignment: MainAxisAlignment.end,
                  ),
                  SizedBox(height: 10),
                ],
              );
            });
      },
      child: Text(
        "Edit",
        style: Theme.of(context).textTheme.body1,
      ),
    );
  }

  InputDecoration getInputDecoration({String hintText, String labelText}) {
    return new InputDecoration(
      hintText: hintText,
      labelText: labelText,
      hintStyle: Theme.of(context).textTheme.body1.merge(
            TextStyle(color: Theme.of(context).focusColor),
          ),
      enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: Theme.of(context).hintColor.withOpacity(0.2))),
      focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Theme.of(context).hintColor)),
      hasFloatingPlaceholder: true,
      labelStyle: Theme.of(context).textTheme.body1.merge(
            TextStyle(color: Theme.of(context).hintColor),
          ),
    );
  }

  _submit() async {

    var res = await _updateProfile(userid);

    try {
      UpdateProfileResponse updateProfileResponse = UpdateProfileResponse.fromJson(res);

      print('_updateProfile() Response' + res.toString());

      if (updateProfileResponse.status == true) {
        print('Profile Update Success: ' + updateProfileResponse.status.toString());

        SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
        sharedPreferences.setString('currentUserId', userid);
        sharedPreferences.setString('name', name);
        sharedPreferences.setString('email', email);
        sharedPreferences.setString('phone', phone);
        sharedPreferences.setString('gender', gender);
        sharedPreferences.setString('dateofbirth', dateofbirth); // to be changed
        sharedPreferences.setString('avtar', avtar);
        sharedPreferences.setString('address', address);

        Navigator.of(context).pop();

      }
    }catch(e){
      _globalKey.currentState.showSnackBar(SnackBar(content: Text('Error Occured')));
    }

  }

  Future<dynamic> _updateProfile(String id) async {
    try {
      Options options = Options(
        contentType: ContentType.parse('application/json'),
      );

      FormData formdata = new FormData(); // just like JS
      formdata.add("userid", id);
      formdata.add("fullname", name);
      formdata.add("email", email);
      formdata.add("contact", phone);
      formdata.add("gender", gender);
      formdata.add("birthdate", dateofbirth);
      formdata.add("profile_picture", avtar);

      var response = await dio.post('updateprofile.php', data: formdata , options: options);

      if (response.statusCode == 200 || response.statusCode == 201) {
        var responseJson = json.decode(response.data);
        return responseJson;
      } else
        showInDialog(context, "Update Error", 'Error: '+response.statusCode.toString());
      print('responseCode: '+ response.statusCode.toString());
    } on DioError catch (exception) {
      if (exception == null || exception.toString().contains('SocketException')) {
        showInDialog(context, "Error", "Network Error");
      } else if (exception.type == DioErrorType.RECEIVE_TIMEOUT ||
          exception.type == DioErrorType.CONNECT_TIMEOUT) {
        showInDialog(
            context,
            "Error",
            "Could'nt connect, please ensure you have a stable network.");
      } else {
        return null;
      }
    }
  }

  void showInDialog(BuildContext context, String title, String value) async {
    showDialog(
      context: context,
      child: new AlertDialog(
        title: Text(title),
        content: Text(value),
        actions: [
          new FlatButton(
            child: const Text("Ok"),
            onPressed: () => Navigator.pop(context),
          ),
        ],
      ),
    );
  }

}
