import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:ecommerce_app_ui_kit/src/responses/SignUpResponse.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';
import '../responses/SignInResponse.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../models/user.dart';

class SocialMediaWidget extends StatefulWidget {
  @override
  _SocialMediaWidgetState createState() => _SocialMediaWidgetState();
}

class _SocialMediaWidgetState extends State<SocialMediaWidget> {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final GoogleSignIn googleSignIn = GoogleSignIn();

  static var uri = "http://qmovement.online/webservices/";

  static BaseOptions options = BaseOptions(
      baseUrl: uri,
      responseType: ResponseType.plain,
      connectTimeout: 30000,
      receiveTimeout: 30000,
      validateStatus: (code) {
        if (code >= 200) {
          return true;
        }
        else{
          return false;
        }
      });

  static Dio dio = Dio(options);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        SizedBox(
          width: 45,
          height: 45,
          child: InkWell(
            onTap: () {
              // signin with fb

              try {
                FacebookLogin().logOut();
                FirebaseAuth.instance.signOut();
              }
              catch(e){}

              _handleSignIn('FB').then((user) async {
                if (user!=null) {

                  // logged in
                  // register user into server with api

                  var res = await _signUp(user.email, '123456');

                  try {
                    SignUpResponse signUpResponse = SignUpResponse.fromJson(res);

                    print('_signUp() Response' + res.toString());

                    if (signUpResponse.status == true) {
                      print('Signup Success: ' + signUpResponse.id.toString());

                      SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
                      sharedPreferences.setString('currentUserId', signUpResponse.id.toString());
                      sharedPreferences.setString('name', user.displayName);
                      sharedPreferences.setString('email', user.email);
                      sharedPreferences.setString('avtar', user.photoUrl);

//                      Navigator.of(context).pushNamed('/SignInMobile');
                      Navigator.of(context).pushNamed('/Tabs',arguments: 0);
                    }else{

                      // user already exists in database

                      var res = await _signIn(user.email, '123456');

                      try {
                        SignInResponse signInResponse = SignInResponse.fromJson(res);

                        print('_signIn() Response' + res.toString());

                        if (signInResponse.status == true) {
                          Scaffold.of(context).showSnackBar(SnackBar(content: Text('Sign In Success'),));

                          String id = signInResponse.userDetails.id;
                          String firstname = signInResponse.userDetails.firstname==null? 'NA': signInResponse.userDetails.firstname ;
                          String lastname = signInResponse.userDetails.lastname==null? 'NA': signInResponse.userDetails.lastname;
                          String name =  firstname+' '+lastname;
                          String email = signInResponse.userDetails.email==null? ' ': signInResponse.userDetails.email;
                          String phone = signInResponse.userDetails.contact==null? ' ': signInResponse.userDetails.contact;
                          String gender = signInResponse.userDetails.gender==null? 'Male': signInResponse.userDetails.gender ;
                          String dob = signInResponse.userDetails.birthdate==null? '2000-01-01': signInResponse.userDetails.birthdate;
                          String avtar = signInResponse.userDetails.profilePicture==null? ' ': signInResponse.userDetails.profilePicture;
                          String address = signInResponse.userDetails.address==null? ' ': signInResponse.userDetails.address;

                          SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
                          sharedPreferences.setString('currentUserId', id);
                          sharedPreferences.setString('name', name);
                          sharedPreferences.setString('email', email);
                          sharedPreferences.setString('phone', phone);
                          sharedPreferences.setString('gender', gender);
                          sharedPreferences.setString('dateofbirth', dob); // to be changed
                          sharedPreferences.setString('avtar', avtar);
                          sharedPreferences.setString('address', address);

                          String date;
                          if(dob!=null) {
                            if (dob.contains(" ")) {
                              date = dob.split(" ")[0];
                            } else {
                              date = dob;
                            }
                          }else{
                            date = dob;
                          }

                          User.advanced(id, name, email, phone, gender, DateTime(int.parse(date.split('-')[0]),int.parse(date.split('-')[1]),int.parse(date.split('-')[2])), avtar, address, UserState.available);

//                          Navigator.of(context).pushNamed('/SignInMobile');
                          Navigator.of(context).pushNamed('/Tabs',arguments: 0);
                        }else{
                          Scaffold.of(context).showSnackBar(SnackBar(content: Text('Error'),));
                        }

                      }catch(e){
                        Scaffold.of(context).showSnackBar(SnackBar(content: Text('Error Occured')));
                        print('Error Occured:'+e.toString());
                      }

                    }



                  }catch(e){
                    print('Error Occured');
                  }


                } else {

                }
              });

            },
            child: Image.asset('img/facebook.png'),
          ),
        ),
        SizedBox(width: 10),
        SizedBox(
          width: 45,
          height: 45,
          child: InkWell(
            onTap: () {
              // Signin with Google

              try {
                signOutGoogle();
                FirebaseAuth.instance.signOut();
              }
              catch(e){}

              signInWithGoogle().then((FirebaseUser user) async {

                // register user into server with api

                var res = await _signUp(user.email, '123456');

                print('_signUp() Response:1' + res.toString());

                try {
                  SignUpResponse signUpResponse = SignUpResponse.fromJson(res);

                  print('_signUp() Response:2' + res.toString());

                  if (signUpResponse.status == true) {
                    print('Signup Success: ' + signUpResponse.id.toString());

                    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
                    sharedPreferences.setString('currentUserId', signUpResponse.id.toString());
                    sharedPreferences.setString('name', user.displayName);
                    sharedPreferences.setString('email', user.email);
                    sharedPreferences.setString('avtar', user.photoUrl);

//                    Navigator.of(context).pushNamed('/SignInMobile');
                    Navigator.of(context).pushNamed('/Tabs',arguments: 0);
                  }else{

                    // user already exists in database

                    var res = await _signIn(user.email, '123456');

                    try {
                      SignInResponse signInResponse = SignInResponse.fromJson(res);

                      print('_signIn() Response' + res.toString());

                      if (signInResponse.status == true) {
                        Scaffold.of(context).showSnackBar(SnackBar(content: Text('Sign In Success'),));

                        String id = signInResponse.userDetails.id;
                        String firstname = signInResponse.userDetails.firstname==null? 'NA': signInResponse.userDetails.firstname ;
                        String lastname = signInResponse.userDetails.lastname==null? 'NA': signInResponse.userDetails.lastname;
                        String name =  firstname+' '+lastname;
                        String email = signInResponse.userDetails.email==null? ' ': signInResponse.userDetails.email;
                        String phone = signInResponse.userDetails.contact==null? ' ': signInResponse.userDetails.contact;
                        String gender = signInResponse.userDetails.gender==null? 'Male': signInResponse.userDetails.gender ;
                        String dob = signInResponse.userDetails.birthdate==null? '2000-01-01': signInResponse.userDetails.birthdate;
                        String avtar = signInResponse.userDetails.profilePicture==null? ' ': signInResponse.userDetails.profilePicture;
                        String address = signInResponse.userDetails.address==null? ' ': signInResponse.userDetails.address;

                        SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
                        sharedPreferences.setString('currentUserId', id);
                        sharedPreferences.setString('name', name);
                        sharedPreferences.setString('email', email);
                        sharedPreferences.setString('phone', phone);
                        sharedPreferences.setString('gender', gender);
                        sharedPreferences.setString('dateofbirth', dob); // to be changed
                        sharedPreferences.setString('avtar', avtar);
                        sharedPreferences.setString('address', address);

                        String date;
                        if(dob!=null) {
                          if (dob.contains(" ")) {
                            date = dob.split(" ")[0];
                          } else {
                            date = dob;
                          }
                        }else{
                          date = dob;
                        }

                        User.advanced(id, name, email, phone, gender, DateTime(int.parse(date.split('-')[0]),int.parse(date.split('-')[1]),int.parse(date.split('-')[2])), avtar, address, UserState.available);

//                        Navigator.of(context).pushNamed('/SignInMobile');
                        Navigator.of(context).pushNamed('/Tabs',arguments: 0);
                      }else{
                        Scaffold.of(context).showSnackBar(SnackBar(content: Text('Error'),));
                      }

                    }catch(e){
                      Scaffold.of(context).showSnackBar(SnackBar(content: Text('Error Occured')));
                      print('Error Occured:'+e.toString());
                    }

                  }

                }catch(e){
                  print('Error Occured:'+e.toString());
                }


              });


            },
            child: Image.asset('img/google-plus.png'),
          ),
        ),
      ],
    );
  }

  Future<FirebaseUser> signInWithGoogle() async {
    final GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
    final GoogleSignInAuthentication googleSignInAuthentication =
    await googleSignInAccount.authentication;

    final AuthCredential credential = GoogleAuthProvider.getCredential(
      accessToken: googleSignInAuthentication.accessToken,
      idToken: googleSignInAuthentication.idToken,
    );

    final AuthResult authResult = await _auth.signInWithCredential(credential);
    final FirebaseUser user = authResult.user;

    assert(!user.isAnonymous);
    assert(await user.getIdToken() != null);

    final FirebaseUser currentUser = await _auth.currentUser();
    assert(user.uid == currentUser.uid);

    return currentUser;

  }

  Future<dynamic> _signUp(String email, String password) async {
    try {
      Options options = Options(
        contentType: ContentType.parse('application/json'),
      );

      FormData formdata = new FormData(); // just like JS
      formdata.add("email", email);
      formdata.add("password", password);

      var response = await dio.post('signup.php', data: formdata , options: options);

      if (response.statusCode == 200 || response.statusCode == 201) {
        var responseJson = json.decode(response.data);
        return responseJson;
      } else
        showInDialog(context, "Registration Error", 'Error: '+response.statusCode.toString());
      print('responseCode: '+ response.statusCode.toString());
    } on DioError catch (exception) {
      if (exception == null || exception.toString().contains('SocketException')) {
        showInDialog(context, "Error", "Network Error");
      } else if (exception.type == DioErrorType.RECEIVE_TIMEOUT ||
          exception.type == DioErrorType.CONNECT_TIMEOUT) {
        showInDialog(
            context,
            "Error",
            "Could'nt connect, please ensure you have a stable network.");
      } else {
        return null;
      }
    }
  }

  void showInDialog(BuildContext context, String title, String value) async {
    showDialog(
      context: context,
      child: new AlertDialog(
        title: Text(title),
        content: Text(value),
        actions: [
          new FlatButton(
            child: const Text("Ok"),
            onPressed: () => Navigator.pop(context),
          ),
        ],
      ),
    );
  }

  Future<FirebaseUser> _handleSignIn(String type) async {
        FacebookLoginResult facebookLoginResult = await _handleFBSignIn();
        final accessToken = facebookLoginResult.accessToken.token;
        if (facebookLoginResult.status == FacebookLoginStatus.loggedIn) {
          final facebookAuthCred = FacebookAuthProvider.getCredential(accessToken: accessToken);
          final user = await FirebaseAuth.instance.signInWithCredential(facebookAuthCred);
          print("User : " + user.user.displayName);
          return user.user;
        }
        else{
          print('_handleSignIn:'+facebookLoginResult.status.toString());
        }
  }

  Future<FacebookLoginResult> _handleFBSignIn() async {
    FacebookLogin facebookLogin = FacebookLogin();
    FacebookLoginResult facebookLoginResult = await facebookLogin.logInWithReadPermissions(['email', 'public_profile']);
    switch (facebookLoginResult.status) {
      case FacebookLoginStatus.cancelledByUser:
        print("Cancelled:"+facebookLoginResult.status.toString());
        break;
      case FacebookLoginStatus.error:
        print("error:"+facebookLoginResult.status.index.toString());
        break;
      case FacebookLoginStatus.loggedIn:
        print("Logged In");
        break;
    }
    return facebookLoginResult;
  }

  Future<dynamic> _signIn(String email, String password) async {
    try {
      Options options = Options(
        contentType: ContentType.parse('application/json'),
      );

      FormData formdata = new FormData(); // just like JS
      formdata.add("email", email);
      formdata.add("password", password);

      var response = await dio.post('login.php', data: formdata , options: options);

      if (response.statusCode == 200 || response.statusCode == 201) {
        var responseJson = json.decode(response.data);
        return responseJson;
      } else
        showInDialog(context, "SignIn Error", 'Error: '+response.statusCode.toString());
      print('responseCode: '+ response.statusCode.toString());
    } on DioError catch (exception) {
      if (exception == null || exception.toString().contains('SocketException')) {
        showInDialog(context, "Error", "Network Error");
      } else if (exception.type == DioErrorType.RECEIVE_TIMEOUT ||
          exception.type == DioErrorType.CONNECT_TIMEOUT) {
        showInDialog(
            context,
            "Error",
            "Could'nt connect, please ensure you have a stable network.");
      } else {
        return null;
      }
    }
  }

  void signOutGoogle() async{
    await googleSignIn.signOut();

    print("User Sign Out");
  }

}
