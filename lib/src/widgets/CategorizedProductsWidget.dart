import 'package:ecommerce_app_ui_kit/src/models/product.dart';
import 'package:ecommerce_app_ui_kit/src/screens/accessories.dart';
import 'package:ecommerce_app_ui_kit/src/widgets/ProductGridItemWidget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

class CategorizedProductsWidget extends StatelessWidget {
  const CategorizedProductsWidget({
    Key key,
    @required this.animationOpacity,
    @required List<Product> productsList,
    @required this.from_page,
  })  : _productsList = productsList,
        super(key: key);

  final Animation animationOpacity;
  final List<Product> _productsList;
  final String from_page;

  @override
  Widget build(BuildContext context) {
    return FadeTransition(
      opacity: animationOpacity,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
        child: new StaggeredGridView.countBuilder(
          primary: false,
          shrinkWrap: true,
          crossAxisCount: 4,
          itemCount: _productsList.length,
          itemBuilder: (BuildContext context, int index) {
            Product product = _productsList.elementAt(index);

            if(from_page=='Home' || from_page=='Home_Category') {
              if (index < 5) {
                return ProductGridItemWidget(
                  product: product,
                  heroTag: 'categorized_products_grid',
                );
              } else if (index == 5) {
                return InkWell(
                  highlightColor: Colors.transparent,
                  splashColor: Theme.of(context).accentColor.withOpacity(0.08),
                  onTap: () {
                    if(from_page=='Home') {
                      Navigator.pop(context);
                      Navigator.of(context).pushNamed('/Tabs', arguments: 1);
                    }

                    if(from_page=='Home_Category') {
                      Navigator.pop(context);
                      Navigator.of(context).push(MaterialPageRoute(builder: (context)=>AccessoriesWidget())); // return to accessories
                    }

                  },
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6),
                      boxShadow: [
                        BoxShadow(color: Theme.of(context).hintColor.withOpacity(0.10), offset: Offset(0, 4), blurRadius: 10)
                      ],
                    ),
                    child: Stack(
                      alignment: AlignmentDirectional.topCenter,
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
                          width: 140,
                          height: 260,
                          decoration: BoxDecoration(
                              color: Theme.of(context).primaryColor,
                              borderRadius: BorderRadius.circular(6),
                              boxShadow: [
                                BoxShadow(
                                    color: Theme.of(context).hintColor.withOpacity(0.15),
                                    offset: Offset(0, 3),
                                    blurRadius: 10)
                              ]),
                          child: Center(child: Text('View More', style: TextStyle(fontSize: 16),)),
                        )
                      ],
                    ),
                  ),
                );
              }
              else {
                return Container();
              }
            }else{
              return ProductGridItemWidget(
                product: product,
                heroTag: 'categorized_products_grid',
              );
            }

          },
//              staggeredTileBuilder: (int index) => new StaggeredTile.count(2, index.isEven ? 2 : 1),
          staggeredTileBuilder: (int index) => new StaggeredTile.fit(2),
          mainAxisSpacing: 15.0,
          crossAxisSpacing: 15.0,
        ),
      ),
    );
  }
}
