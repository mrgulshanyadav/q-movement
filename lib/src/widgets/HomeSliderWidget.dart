import 'dart:convert';
import 'dart:io';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:dio/dio.dart';
import 'package:ecommerce_app_ui_kit/src/responses/HomeBanner.dart';
import 'package:flutter/material.dart';

class HomeSliderWidget extends StatefulWidget {
  @override
  _HomeSliderWidgetState createState() => _HomeSliderWidgetState();
}

class _HomeSliderWidgetState extends State<HomeSliderWidget> {
  int _current = 0;

  static var uri = "http://qmovement.online/webservices/";

  static BaseOptions options = BaseOptions(
      baseUrl: uri,
      responseType: ResponseType.plain,
      connectTimeout: 30000,
      receiveTimeout: 30000,
      validateStatus: (code) {
        if (code >= 200) {
          return true;
        }
        else{
          return false;
        }
      });

  static Dio dio = Dio(options);

  HomeBanner homeBanner;

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: AlignmentDirectional.bottomEnd,
//      fit: StackFit.expand,
      children: <Widget>[

        FutureBuilder(
          future: getHomeBanner(),
          builder: (context,res){

            if (!res.hasData) {
              return CarouselSlider(
                  autoPlay: true,
                  autoPlayInterval: Duration(seconds: 5),
                  height: 240,
                  viewportFraction: 1.0,
                  onPageChanged: (index) {
                    setState(() {
                      _current = index;
                    });
                  },
                  items: [
                    Center(child: CircularProgressIndicator())
                  ]
              );
            } else {

              print("Response2: "+res.data.toString());

              homeBanner = HomeBanner.fromJson(res.data);

              return CarouselSlider(
                autoPlay: true,
                autoPlayInterval: Duration(seconds: 5),
                height: 240,
                viewportFraction: 1.0,
                onPageChanged: (index) {
                  setState(() {
                    _current = index;
                  });
                },
                items: homeBanner.banner.map((img) {
                  return Builder(
                    builder: (BuildContext context) {
                      return Container(
                        margin: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                        height: 200,
                        decoration: BoxDecoration(
                          image: DecorationImage(image: NetworkImage(img), fit: BoxFit.cover),
                          borderRadius: BorderRadius.circular(6),
                        ),
                      );
                    },
                  );
                }).toList(),
              );
            }

          },
        ),

        FutureBuilder(
          future: getHomeBanner(),
          builder: (context,res){

            if (!res.hasData) {
              return Center(child: CircularProgressIndicator());
            } else {

              print("Response2: "+res.data.toString());

              homeBanner = HomeBanner.fromJson(res.data);

              return Positioned(
                bottom: 25,
                right: 41,
//          width: config.App(context).appWidth(100),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: homeBanner.banner.map((slide) {
                    return Container(
                      width: 20.0,
                      height: 3.0,
                      margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(
                            Radius.circular(8),
                          ),
                          color: _current == homeBanner.banner.indexOf(slide)
                              ? Theme.of(context).hintColor
                              : Theme.of(context).hintColor.withOpacity(0.3)),
                    );
                  }).toList(),
                ),
              );
            }

          },
        ),

      ],
    );
  }

  Future<void> getHomeBanner() async {
    try {
      Options options = Options(
        contentType: ContentType.parse('application/json'),
      );

      var response = await dio.get('homebanner.php', options: options);

      if (response.statusCode == 200 || response.statusCode == 201) {
        var responseJson = json.decode(response.data);

        print("Response: "+responseJson.toString());

        return responseJson;
      } else
        print("Error: "+response.statusCode.toString());
    } on DioError catch (exception) {
      if (exception == null ||
          exception.toString().contains('SocketException')) {
        print("Error: "+"Network Error");
      } else if (exception.type == DioErrorType.RECEIVE_TIMEOUT ||
          exception.type == DioErrorType.CONNECT_TIMEOUT) {
        print("Error: "+ "Could'nt connect, please ensure you have a stable network.");
      } else {
        return null;
      }
    }
  }

}
