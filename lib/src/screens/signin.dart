import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:ecommerce_app_ui_kit/config/ui_icons.dart';
import 'package:ecommerce_app_ui_kit/src/models/user.dart';
import 'package:ecommerce_app_ui_kit/src/responses/SignInResponse.dart';
import 'package:ecommerce_app_ui_kit/src/responses/SignUpResponse.dart';
import 'package:ecommerce_app_ui_kit/src/widgets/SocialMediaWidget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SignInWidget extends StatefulWidget {
  @override
  _SignInWidgetState createState() => _SignInWidgetState();
}

class _SignInWidgetState extends State<SignInWidget> {
  bool _showPassword = false;
  String email, password;

  GlobalKey<ScaffoldState> _globalKey = GlobalKey();

  static var uri = "http://qmovement.online/webservices/";

  static BaseOptions options = BaseOptions(
      baseUrl: uri,
      responseType: ResponseType.plain,
      connectTimeout: 30000,
      receiveTimeout: 30000,
      validateStatus: (code) {
        if (code >= 200) {
          return true;
        }
        else{
          return false;
        }
      });

  static Dio dio = Dio(options);

  bool isLogged;

  isUserLogged() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      isLogged = prefs.getBool('isLogged');
    });
    return {'status':true};
  }

  @override
  void initState() {

    email = '';
    password = '';

    isLogged = false;
    isUserLogged();

    super.initState();
  }

  Widget build(BuildContext context) {

    if(isLogged==true){
      Navigator.of(context).pushNamed('/Tabs',arguments: 0);
    }

    return Scaffold(
      key: _globalKey,
      backgroundColor: Theme.of(context).accentColor,
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
                Container(
                  width: double.infinity,
                  padding: EdgeInsets.symmetric(vertical: 30, horizontal: 20),
                  margin: EdgeInsets.symmetric(vertical: 65, horizontal: 50),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Theme.of(context).primaryColor.withOpacity(0.6),
                  ),
                ),
                Container(
                  width: double.infinity,
                  padding: EdgeInsets.symmetric(vertical: 30, horizontal: 30),
                  margin: EdgeInsets.symmetric(vertical: 85, horizontal: 20),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Theme.of(context).primaryColor,
                    boxShadow: [
                      BoxShadow(
                          color: Theme.of(context).hintColor.withOpacity(0.2), offset: Offset(0, 10), blurRadius: 20)
                    ],
                  ),
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: 25),
                      Text('Sign In', style: Theme.of(context).textTheme.display3),
                      SizedBox(height: 20),
                      new TextField(
                        style: TextStyle(color: Theme.of(context).accentColor),
                        keyboardType: TextInputType.emailAddress,
                        decoration: new InputDecoration(
                          hintText: 'Email Address',
                          hintStyle: Theme.of(context).textTheme.body1.merge(
                            TextStyle(color: Theme.of(context).accentColor),
                          ),
                          enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Theme.of(context).accentColor.withOpacity(0.2))),
                          focusedBorder:
                          UnderlineInputBorder(borderSide: BorderSide(color: Theme.of(context).accentColor)),
                          prefixIcon: Icon(
                            UiIcons.envelope,
                            color: Theme.of(context).accentColor,
                          ),
                        ),
                        onChanged: (val){
                          setState(() {
                            email = val;
                          });

                        },
                      ),
                      SizedBox(height: 20),
                      new TextField(
                        style: TextStyle(color: Theme.of(context).accentColor),
                        keyboardType: TextInputType.text,
                        obscureText: !_showPassword,
                        decoration: new InputDecoration(
                          hintText: 'Password',
                          hintStyle: Theme.of(context).textTheme.body1.merge(
                            TextStyle(color: Theme.of(context).accentColor),
                          ),
                          enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Theme.of(context).accentColor.withOpacity(0.2))),
                          focusedBorder:
                          UnderlineInputBorder(borderSide: BorderSide(color: Theme.of(context).accentColor)),
                          prefixIcon: Icon(
                            UiIcons.padlock_1,
                            color: Theme.of(context).accentColor,
                          ),
                          suffixIcon: IconButton(
                            onPressed: () {
                              setState(() {
                                _showPassword = !_showPassword;
                              });
                            },
                            color: Theme.of(context).accentColor.withOpacity(0.4),
                            icon: Icon(_showPassword ? Icons.visibility_off : Icons.visibility),
                          ),
                        ),
                        onChanged: (val){
                          setState(() {
                            password = val;
                          });
                        },
                      ),
                      SizedBox(height: 40),
                      FlatButton(
                        padding: EdgeInsets.symmetric(vertical: 12, horizontal: 70),
                        onPressed: () async {

                          if(!email.contains('@') || email.isEmpty){
                            _globalKey.currentState.showSnackBar(SnackBar(content: Text('Invalid Email!'),));
                          }
                          else if(password.isEmpty){
                            _globalKey.currentState.showSnackBar(SnackBar(content: Text('Enter Password!'),));
                          }
                          else{
                            var res = await _signIn(email, password);

                            try {
                              SignInResponse signInResponse = SignInResponse.fromJson(res);

                              print('_signIn() Response' + res.toString());

                              if (signInResponse.status == true) {
                                _globalKey.currentState.showSnackBar(SnackBar(content: Text('Sign In Success'),));

                                String id = signInResponse.userDetails.id;
                                String firstname = signInResponse.userDetails.firstname==null? 'NA': signInResponse.userDetails.firstname ;
                                String lastname = signInResponse.userDetails.lastname==null? 'NA': signInResponse.userDetails.lastname;
                                String name =  firstname+' '+lastname;
                                String email = signInResponse.userDetails.email==null? ' ': signInResponse.userDetails.email;
                                String phone = signInResponse.userDetails.contact==null? ' ': signInResponse.userDetails.contact;
                                String gender = signInResponse.userDetails.gender==null? 'Male': signInResponse.userDetails.gender ;
                                String dob = signInResponse.userDetails.birthdate==null? '2000-01-01': signInResponse.userDetails.birthdate;
                                String avtar = signInResponse.userDetails.profilePicture==null? ' ': signInResponse.userDetails.profilePicture;
                                String address = signInResponse.userDetails.address==null? ' ': signInResponse.userDetails.address;

                                SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
                                sharedPreferences.setString('currentUserId', id);
                                sharedPreferences.setString('name', name);
                                sharedPreferences.setString('email', email);
                                sharedPreferences.setString('phone', phone);
                                sharedPreferences.setString('gender', gender);
                                sharedPreferences.setString('dateofbirth', dob); // to be changed
                                sharedPreferences.setString('avtar', avtar);
                                sharedPreferences.setString('address', address);
                                sharedPreferences.setBool('isLogged', true);

                                String date;
                                if(dob!=null) {
                                  if (dob.contains(" ")) {
                                    date = dob.split(" ")[0];
                                  } else {
                                    date = dob;
                                  }
                                }else{
                                  date = dob;
                                }

                                User.advanced(id, name, email, phone, gender, DateTime(int.parse(date.split('-')[0]),int.parse(date.split('-')[1]),int.parse(date.split('-')[2])), avtar, address, UserState.available);

                                Navigator.of(context).pushNamed('/Tabs',arguments: 0);
                              }else{
                                _globalKey.currentState.showSnackBar(SnackBar(content: Text('Error'),));
                              }

                            }catch(e){
                              _globalKey.currentState.showSnackBar(SnackBar(content: Text('Error Occured')));
                              print('Error Occured:'+e.toString());
                            }

                          }

                        },
                        child: Text(
                          'Sign In',
                          style: Theme.of(context).textTheme.title.merge(
                            TextStyle(color: Theme.of(context).primaryColor),
                          ),
                        ),
                        color: Theme.of(context).accentColor,
                        shape: StadiumBorder(),
                      ),
                      SizedBox(height: 50),
                      Text(
                        'Or using social media',
                        style: Theme.of(context).textTheme.body1,
                      ),
                      SizedBox(height: 20),
                      new SocialMediaWidget()
                    ],
                  ),
                ),
              ],
            ),
            FlatButton(
              onPressed: () {
                Navigator.of(context).pushNamed('/SignUp');
              },
              child: RichText(
                text: TextSpan(
                  style: Theme.of(context).textTheme.title.merge(
                    TextStyle(color: Theme.of(context).primaryColor),
                  ),
                  children: [
                    TextSpan(text: 'Don\'t have an account ?'),
                    TextSpan(text: ' Sign Up', style: TextStyle(fontWeight: FontWeight.w700)),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<dynamic> _signIn(String email, String password) async {
    try {
      Options options = Options(
        contentType: ContentType.parse('application/json'),
      );

      FormData formdata = new FormData(); // just like JS
      formdata.add("email", email);
      formdata.add("password", password);

      var response = await dio.post('login.php', data: formdata , options: options);

      if (response.statusCode == 200 || response.statusCode == 201) {
        var responseJson = json.decode(response.data);
        return responseJson;
      } else
        showInDialog(context, "SignIn Error", 'Error: '+response.statusCode.toString());
      print('responseCode: '+ response.statusCode.toString());
    } on DioError catch (exception) {
      if (exception == null || exception.toString().contains('SocketException')) {
        showInDialog(context, "Error", "Network Error");
      } else if (exception.type == DioErrorType.RECEIVE_TIMEOUT ||
          exception.type == DioErrorType.CONNECT_TIMEOUT) {
        showInDialog(
            context,
            "Error",
            "Could'nt connect, please ensure you have a stable network.");
      } else {
        return null;
      }
    }
  }

  void showInDialog(BuildContext context, String title, String value) async {
    showDialog(
      context: context,
      child: new AlertDialog(
        title: Text(title),
        content: Text(value),
        actions: [
          new FlatButton(
            child: const Text("Ok"),
            onPressed: () => Navigator.of(context).pop(),
          ),
        ],
      ),
    );
  }

}
