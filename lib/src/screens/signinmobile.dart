import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:ecommerce_app_ui_kit/config/ui_icons.dart';
import 'package:ecommerce_app_ui_kit/src/widgets/SocialMediaWidget.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:google_sign_in/google_sign_in.dart';
import '../responses/UpdateProfileResponse.dart';


class SignInMobileWidget extends StatefulWidget {
  @override
  _SignInMobileWidgetState createState() => _SignInMobileWidgetState();
}

class _SignInMobileWidgetState extends State<SignInMobileWidget> {
  bool _showPassword = false;

  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();

  String phoneNo;
  String smsOTP;
  String verificationId;
  String errorMessage = '';
  FirebaseAuth _auth = FirebaseAuth.instance;

  static var uri = "http://qmovement.online/webservices/";

  static BaseOptions options = BaseOptions(
      baseUrl: uri,
      responseType: ResponseType.plain,
      connectTimeout: 30000,
      receiveTimeout: 30000,
      validateStatus: (code) {
        if (code >= 200) {
          return true;
        }
        else{
          return false;
        }
      });

  static Dio dio = Dio(options);

  String userid, name, email, phone, gender, dateofbirth, avtar, address;

  getUserDetails() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      userid = prefs.getString('currentUserId');
      name = prefs.getString('name');
      email = prefs.getString('email');
      phone = prefs.getString('phone')?? '0';
      gender = prefs.getString('gender')?? 'Male';
      dateofbirth = prefs.getString('dateofbirth')?? '2000-1-1';
      avtar = prefs.getString('avtar')??'http://';
      address = prefs.getString('address')??' ';
    });
    return {'status':true};
  }

  @override
  void initState() {

    getUserDetails();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).accentColor,
      key: _globalKey,
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
                Container(
                  width: double.infinity,
                  padding: EdgeInsets.symmetric(vertical: 30, horizontal: 20),
                  margin: EdgeInsets.symmetric(vertical: 65, horizontal: 50),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Theme.of(context).primaryColor.withOpacity(0.6),
                  ),
                ),
                Container(
                  width: double.infinity,
                  padding: EdgeInsets.symmetric(vertical: 30, horizontal: 30),
                  margin: EdgeInsets.symmetric(vertical: 85, horizontal: 20),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Theme.of(context).primaryColor,
                      boxShadow: [
                        BoxShadow(
                            color: Theme.of(context).hintColor.withOpacity(0.2), offset: Offset(0, 10), blurRadius: 20)
                      ]),
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: 25),
                      Text('Sign In', style: Theme.of(context).textTheme.display3),
                      SizedBox(height: 20),
                      new TextField(
                        style: TextStyle(color: Theme.of(context).accentColor),
                        keyboardType: TextInputType.number,
                        decoration: new InputDecoration(
                          hintText: 'Enter mobile number',
                          hintStyle: Theme.of(context).textTheme.body1.merge(
                            TextStyle(color: Theme.of(context).accentColor),
                          ),
                          counterText: '',
                          enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Theme.of(context).accentColor.withOpacity(0.2))),
                          focusedBorder:
                          UnderlineInputBorder(borderSide: BorderSide(color: Theme.of(context).accentColor)),
                          prefixIcon: Icon(
                            UiIcons.phone_call,
                            color: Theme.of(context).accentColor,
                          ),
                        ),
                        maxLength: 15,
                        onChanged: (input){
                          setState(() {
                            phoneNo = input;
                          });
                        },
                      ),
                      SizedBox(height: 20),
                      FlatButton(
                        padding: EdgeInsets.symmetric(vertical: 12, horizontal: 70),
                        onPressed: () {
                          // 2 number refer the index of Home page

                          verifyPhone();

                        },
                        child: Text(
                          'Continue',
                          style: Theme.of(context).textTheme.title.merge(
                            TextStyle(color: Theme.of(context).primaryColor),
                          ),
                        ),
                        color: Theme.of(context).accentColor,
                        shape: StadiumBorder(),
                      ),
                      SizedBox(height: 20),
                    ],
                  ),
                ),
              ],
            ),
            FlatButton(
              onPressed: () {
                Navigator.of(context).pushNamed('/SignUp');
              },
              child: RichText(
                text: TextSpan(
                  style: Theme.of(context).textTheme.title.merge(
                    TextStyle(color: Theme.of(context).primaryColor),
                  ),
                  children: [
                    TextSpan(text: 'Don\'t have an account ?'),
                    TextSpan(text: ' Sign Up', style: TextStyle(fontWeight: FontWeight.w700)),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> verifyPhone() async {
    final PhoneCodeSent smsOTPSent = (String verId, [int forceCodeResend]) {
      setState(() {
        verificationId = verId;
      });
      print('verificationId:'+verificationId);
      smsOTPDialog(context).then((value) {
        print('sign in');
      });
    };

    try {
      await _auth.verifyPhoneNumber(
          phoneNumber: '+91'+this.phoneNo, // PHONE NUMBER TO SEND OTP  // country code
          codeAutoRetrievalTimeout: (String verId) {
            //Starts the phone number verification process for the given phone number.
            //Either sends an SMS with a 6 digit code to the phone number specified, or sign's the user in and [verificationCompleted] is called.
            setState(() {
              verificationId = verId;
            });
            print('verificationId:'+verificationId);
          },
          codeSent: smsOTPSent, // WHEN CODE SENT THEN WE OPEN DIALOG TO ENTER OTP.
          timeout: const Duration(seconds: 20),
          verificationCompleted: (AuthCredential phoneAuthCredential) {
            print(phoneAuthCredential);
          },
          verificationFailed: (AuthException exceptio) {
            print('${exceptio.message}');
          });
    } catch (e) {
      handleError(e);
    }
  }

  Future<bool> smsOTPDialog(BuildContext context) {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          print('verificationId[smsOTPDialog]:'+verificationId);
          return new AlertDialog(
            title: Text('Enter SMS Code'),
            content: Container(
              height: 85,
              child: Column(children: [
                TextField(
                  onChanged: (value) {
                    setState(() {
                      smsOTP = value;
                    });
                    print('smsOTP:'+smsOTP);
                  },
                ),
                errorMessage != '' ? Text(errorMessage, style: TextStyle(color: Colors.red),) : Container(),
              ]),
            ),
            contentPadding: EdgeInsets.all(10),
            actions: <Widget>[
              FlatButton(
                child: Text('Done'),
                onPressed: () {
                  if(verificationId==smsOTP) {
                    _auth.currentUser().then((user) async {
                      if (user != null) {

                        var res = await _updateProfile(userid,name,email,phoneNo,avtar);

                        try {
                          UpdateProfileResponse updateProfileResponse = UpdateProfileResponse.fromJson(res);

                          print('_updateProfile() Response' + res.toString());

                          if (updateProfileResponse.status == true) {
                            print('Profile Update Success: ' + updateProfileResponse.status.toString());

                            SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
                            sharedPreferences.setString('currentUserId', userid);
                            sharedPreferences.setString('name', user.displayName);
                            sharedPreferences.setString('email', user.email);
                            sharedPreferences.setString('avtar', user.photoUrl);

                            sharedPreferences.setString('phone', phoneNo);
                            sharedPreferences.setString('gender', gender);
                            sharedPreferences.setString('dateofbirth', dateofbirth); // to be changed
                            sharedPreferences.setString('address', address);

                            Navigator.of(context).pop();
                            Navigator.of(context).pushNamed('/Tabs', arguments: 0);

                          }
                        }catch(e){
                          _globalKey.currentState.showSnackBar(SnackBar(content: Text('Error Occured')));
                        }

                      } else {
                        signIn();
                      }
                    });
                  }else{
                    Navigator.of(context).pop();
                    _globalKey.currentState.showSnackBar(SnackBar(content: Text('Invalid OTP'),duration: Duration(seconds: 10),));
                  }
                },
              )
            ],
          );
        });
  }

  signIn() async {
    try {
      final AuthCredential credential = PhoneAuthProvider.getCredential(
        verificationId: verificationId,
        smsCode: smsOTP,
      );
      await _auth.signInWithCredential(credential).then((authcred) async {
        final FirebaseUser currentUser = await _auth.currentUser();
        assert(authcred.user.uid == currentUser.uid);
        Navigator.of(context).pop();
        Navigator.of(context).pushNamed('/Tabs',arguments: 0);
      });
    } catch (e) {
      handleError(e);
    }
  }

  handleError(PlatformException error) {
    print(error);
    switch (error.code) {
      case 'ERROR_INVALID_VERIFICATION_CODE':
        FocusScope.of(context).requestFocus(new FocusNode());
        setState(() {
          errorMessage = 'Invalid Code';
        });
        print('errorMessage:'+errorMessage);
        Navigator.of(context).pop();
        smsOTPDialog(context).then((value) {
          print('sign in');
        });
        break;
      default:
        setState(() {
          errorMessage = error.message;
        });

        break;
    }
  }

  final GoogleSignIn googleSignIn = GoogleSignIn();

  void signOutGoogle() async{
    await googleSignIn.signOut();

    print("User Sign Out");
  }


  Future<dynamic> _updateProfile(String id, String name, String email, String phone, String avtar) async {
    try {
      Options options = Options(
        contentType: ContentType.parse('application/json'),
      );

      FormData formdata = new FormData(); // just like JS
      formdata.add("userid", id);
      formdata.add("fullname", name);
      formdata.add("email", email);
      formdata.add("contact", phone);
      formdata.add("profile_picture", avtar);

      var response = await dio.post('updateprofile.php', data: formdata , options: options);

      if (response.statusCode == 200 || response.statusCode == 201) {
        var responseJson = json.decode(response.data);
        return responseJson;
      } else
        showInDialog(context, "Update Error", 'Error: '+response.statusCode.toString());
      print('responseCode: '+ response.statusCode.toString());
    } on DioError catch (exception) {
      if (exception == null || exception.toString().contains('SocketException')) {
        showInDialog(context, "Error", "Network Error");
      } else if (exception.type == DioErrorType.RECEIVE_TIMEOUT ||
          exception.type == DioErrorType.CONNECT_TIMEOUT) {
        showInDialog(
            context,
            "Error",
            "Could'nt connect, please ensure you have a stable network.");
      } else {
        return null;
      }
    }
  }

  void showInDialog(BuildContext context, String title, String value) async {
    showDialog(
      context: context,
      child: new AlertDialog(
        title: Text(title),
        content: Text(value),
        actions: [
          new FlatButton(
            child: const Text("Ok"),
            onPressed: () => Navigator.pop(context),
          ),
        ],
      ),
    );
  }


}


