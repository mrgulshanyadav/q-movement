import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:ecommerce_app_ui_kit/config/ui_icons.dart';
import 'package:ecommerce_app_ui_kit/src/responses/CategoryResponse.dart';
import 'package:ecommerce_app_ui_kit/src/responses/BrandResponse.dart';
import 'package:ecommerce_app_ui_kit/src/responses/ConditionResponse.dart';
import 'package:ecommerce_app_ui_kit/src/responses/MaterialResponse.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:grouped_buttons/grouped_buttons.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PostAdWidget extends StatefulWidget {
  @override
  _PostAdWidgetState createState() => _PostAdWidgetState();
}

class _PostAdWidgetState extends State<PostAdWidget> {
  List<String> categoryList;
  List<String> brandList;
  List<String> materialList;
  List<String> conditionList;

  Map<dynamic,dynamic> categoryNameIdMap;
  Map<dynamic,dynamic> brandNameIdMap;
  Map<dynamic,dynamic> materialNameIdMap;
  Map<dynamic,dynamic> conditionNameIdMap;

  String category,brand,brand_name,model_name,new_old,material,type,color,price,fixed_negotiable,contact_number,whatsapp_number,description;
  String old_month,old_year,condition;
  String isWithCase,isUnderWarranty,howMuchTimeRemaining,isOriginalRceipt;


  File _frontimage,_backImage,_rightImage,_leftImage,_boxAndPaperImage;

  Future getFrontImage(BuildContext context) async {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
              content: SizedBox(
                height: 150,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    FlatButton(
                      child: Text(
                        "Camera",
                        style: TextStyle(fontSize: 26),
                      ),
                      onPressed: () async {
                        var image = await ImagePicker.pickImage(source: ImageSource.camera);
                        setState(() {
                          _frontimage = image;
                          Navigator.pop(context);
                        });
                      },
                    ),
                    FlatButton(
                      child: Text(
                        "Gallery",
                        style: TextStyle(fontSize: 26),
                      ),
                      onPressed: () async {
                        var image = await ImagePicker.pickImage(
                            source: ImageSource.gallery);
                        setState(() {
                          _frontimage = image;
                          Navigator.pop(context);
                        });
                      },
                    ),
                    _frontimage != null
                        ? FlatButton(
                      child: Text(
                        "Remove Profile",
                        style: TextStyle(fontSize: 26),
                      ),
                      onPressed: () async {
                        setState(() {
                          _frontimage = null;
                          Navigator.pop(context);
                        });
                      },
                    )
                        : Text("",),
                  ],
                ),
              ));
        });
  }
  Future getBackImage(BuildContext context) async {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
              content: SizedBox(
                height: 150,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    FlatButton(
                      child: Text(
                        "Camera",
                        style: TextStyle(fontSize: 26),
                      ),
                      onPressed: () async {
                        var image = await ImagePicker.pickImage(source: ImageSource.camera);
                        setState(() {
                          _backImage = image;
                          Navigator.pop(context);
                        });
                      },
                    ),
                    FlatButton(
                      child: Text(
                        "Gallery",
                        style: TextStyle(fontSize: 26),
                      ),
                      onPressed: () async {
                        var image = await ImagePicker.pickImage(
                            source: ImageSource.gallery);
                        setState(() {
                          _backImage = image;
                          Navigator.pop(context);
                        });
                      },
                    ),
                    _backImage != null
                        ? FlatButton(
                      child: Text(
                        "Remove Profile",
                        style: TextStyle(fontSize: 26),
                      ),
                      onPressed: () async {
                        setState(() {
                          _backImage = null;
                          Navigator.pop(context);
                        });
                      },
                    )
                        : Text("",),
                  ],
                ),
              ));
        });
  }
  Future getRightImage(BuildContext context) async {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
              content: SizedBox(
                height: 150,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    FlatButton(
                      child: Text(
                        "Camera",
                        style: TextStyle(fontSize: 26),
                      ),
                      onPressed: () async {
                        var image = await ImagePicker.pickImage(source: ImageSource.camera);
                        setState(() {
                          _rightImage = image;
                          Navigator.pop(context);
                        });
                      },
                    ),
                    FlatButton(
                      child: Text(
                        "Gallery",
                        style: TextStyle(fontSize: 26),
                      ),
                      onPressed: () async {
                        var image = await ImagePicker.pickImage(
                            source: ImageSource.gallery);
                        setState(() {
                          _rightImage = image;
                          Navigator.pop(context);
                        });
                      },
                    ),
                    _rightImage != null
                        ? FlatButton(
                      child: Text(
                        "Remove Profile",
                        style: TextStyle(fontSize: 26),
                      ),
                      onPressed: () async {
                        setState(() {
                          _rightImage = null;
                          Navigator.pop(context);
                        });
                      },
                    )
                        : Text("",),
                  ],
                ),
              ));
        });
  }
  Future getLeftImage(BuildContext context) async {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
              content: SizedBox(
                height: 150,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    FlatButton(
                      child: Text(
                        "Camera",
                        style: TextStyle(fontSize: 26),
                      ),
                      onPressed: () async {
                        var image = await ImagePicker.pickImage(source: ImageSource.camera);
                        setState(() {
                          _leftImage = image;
                          Navigator.pop(context);
                        });
                      },
                    ),
                    FlatButton(
                      child: Text(
                        "Gallery",
                        style: TextStyle(fontSize: 26),
                      ),
                      onPressed: () async {
                        var image = await ImagePicker.pickImage(
                            source: ImageSource.gallery);
                        setState(() {
                          _leftImage = image;
                          Navigator.pop(context);
                        });
                      },
                    ),
                    _leftImage != null
                        ? FlatButton(
                      child: Text(
                        "Remove Profile",
                        style: TextStyle(fontSize: 26),
                      ),
                      onPressed: () async {
                        setState(() {
                          _leftImage = null;
                          Navigator.pop(context);
                        });
                      },
                    )
                        : Text("",),
                  ],
                ),
              ));
        });
  }
  Future getBoxAndPaperImage(BuildContext context) async {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
              content: SizedBox(
                height: 150,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    FlatButton(
                      child: Text(
                        "Camera",
                        style: TextStyle(fontSize: 26),
                      ),
                      onPressed: () async {
                        var image = await ImagePicker.pickImage(source: ImageSource.camera);
                        setState(() {
                          _boxAndPaperImage = image;
                          Navigator.pop(context);
                        });
                      },
                    ),
                    FlatButton(
                      child: Text(
                        "Gallery",
                        style: TextStyle(fontSize: 26),
                      ),
                      onPressed: () async {
                        var image = await ImagePicker.pickImage(
                            source: ImageSource.gallery);
                        setState(() {
                          _boxAndPaperImage = image;
                          Navigator.pop(context);
                        });
                      },
                    ),
                    _boxAndPaperImage != null
                        ? FlatButton(
                      child: Text(
                        "Remove Profile",
                        style: TextStyle(fontSize: 26),
                      ),
                      onPressed: () async {
                        setState(() {
                          _boxAndPaperImage = null;
                          Navigator.pop(context);
                        });
                      },
                    )
                        : Text("",),
                  ],
                ),
              ));
        });
  }

  static var uri = "http://qmovement.online/webservices/";

  static BaseOptions options = BaseOptions(
      baseUrl: uri,
      responseType: ResponseType.plain,
      connectTimeout: 30000,
      receiveTimeout: 30000,
      validateStatus: (code) {
        if (code >= 200) {
          return true;
        }
        else{
          return false;
        }
      });

  static Dio dio = Dio(options);

  String userid, name, email, phone, gender, dateofbirth, avtar, address;

  getUserDetails() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      userid = prefs.getString('currentUserId');
      name = prefs.getString('name');
      email = prefs.getString('email');
      phone = prefs.getString('phone');
      gender = prefs.getString('gender');
      dateofbirth = prefs.getString('dateofbirth');
      avtar = prefs.getString('avtar');
      address = prefs.getString('address');
    });
    return {'status':true};
  }

  @override
  void initState() {

    getUserDetails();

    categoryList = [];
    brandList = [];
    brand_name = '';
    model_name = "";
    new_old = "New";
    materialList = [];
    fixed_negotiable = "Fixed";
    conditionList = [];
    isWithCase = "Yes";
    isUnderWarranty = "Yes";
    isOriginalRceipt = "Yes";

    _frontimage =null;
    _backImage = null;
    _rightImage = null;
    _leftImage = null;
    _boxAndPaperImage = null;

    categoryNameIdMap = Map<dynamic,dynamic>();
    brandNameIdMap = Map<dynamic,dynamic>();
    materialNameIdMap = Map<dynamic,dynamic>();
    conditionNameIdMap = Map<dynamic,dynamic>();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ListView(
        children: <Widget>[
          Container(
              margin: EdgeInsets.only(left: 10),
              child: FutureBuilder(
                future: getCategoryList(),
                builder: (context,res){

                  if (!res.hasData) {
                    return Center(child: CircularProgressIndicator());
                  } else {

                    print("Response22: "+res.data.toString());

                    CategoryResponse categoryResponse = CategoryResponse.fromJson(res.data);

                    categoryList.clear();

                    for(int i=0;i<categoryResponse.categories.length;i++){
                      categoryNameIdMap.putIfAbsent(categoryResponse.categories[i].name, ()=> categoryResponse.categories[i].id);
                      categoryList.add(categoryResponse.categories[i].name);
                    }

                    print("categoryResponse: "+categoryResponse.toString());

                    print("categoryNameIdMap: "+categoryNameIdMap.toString());

                    return DropdownButtonHideUnderline(
                        child: DropdownButton<String>(
                          value: category,
                          hint: Text("Category"),
                          icon: Icon(Icons.keyboard_arrow_down),
                          iconSize: 24,
                          onChanged: (String newValue) {
                            setState(() {
                              category = newValue;
                            });
                          },
                          items: categoryList.map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),

                        )
                    );
                  }

                },
              )
          ),
          Visibility(
            visible: category=='Watches'? true : false,
            child: FutureBuilder(
              future: getBrandList(),
              builder: (context,res){

                if (!res.hasData) {
                  return Center(child: CircularProgressIndicator());
                } else {

                  print("Response22: "+res.data.toString());

                  BrandResponse brandResponse = BrandResponse.fromJson(res.data);

                  brandList.clear();

                  for(int i=0;i<brandResponse.categories.length;i++){
                    brandNameIdMap.putIfAbsent(brandResponse.categories[i].nameEng, ()=> brandResponse.categories[i].id);
                    brandList.add(brandResponse.categories[i].nameEng);
                  }

                  print("brandResponse: "+brandResponse.toString());

                  print("brandNameIdMap: "+brandNameIdMap.toString());

                  return Container(
                      margin: EdgeInsets.only(left: 10),
                      child: DropdownButtonHideUnderline(
                          child: DropdownButton<String>(
                            value: brand,
                            hint: Text("Brand"),
                            icon: Icon(Icons.keyboard_arrow_down),
                            iconSize: 24,
                            onChanged: (String newValue) {
                              setState(() {
                                brand = newValue;
                              });
                            },
                            items: brandList.map<DropdownMenuItem<String>>((String value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Text(value),
                              );
                            }).toList(),

                          )
                      )
                  );
                }

              },
            ),
          ),
          Visibility(
            visible: category=='Watches'? false : true,
            child: Container(
              margin: EdgeInsets.only(left: 10,right: 10),
              child: new TextFormField(
                style: TextStyle(color: Theme.of(context).accentColor),
                keyboardType: TextInputType.text,
                initialValue: brand_name,
                decoration: new InputDecoration(
                  hintText: 'Brand Name',
                  hintStyle: Theme.of(context).textTheme.body1.merge(
                    TextStyle(color: Theme.of(context).accentColor),
                  ),
                  enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: Theme.of(context).accentColor.withOpacity(0.2))),
                  focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Theme.of(context).accentColor)),
                ),
                onChanged: (input){
                  setState(() {
                    brand_name = input;
                  });
                },
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 10,right: 10),
            child: new TextFormField(
              style: TextStyle(color: Theme.of(context).accentColor),
              keyboardType: TextInputType.text,
              initialValue: model_name,
              decoration: new InputDecoration(
                hintText: 'Model Name',
                hintStyle: Theme.of(context).textTheme.body1.merge(
                  TextStyle(color: Theme.of(context).accentColor),
                ),
                enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: Theme.of(context).accentColor.withOpacity(0.2))),
                focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Theme.of(context).accentColor)),
              ),
              onChanged: (input){
                setState(() {
                  model_name = input;
                });
              },
            ),
          ),

          const SizedBox(
            height: 10,
          ),
          RadioButtonGroup(
            orientation: GroupedButtonsOrientation.HORIZONTAL,
            margin: const EdgeInsets.only(left: 0.0),
            onSelected: (String selected) => setState((){
              new_old = selected;
            }),
            labels: <String>[
              "New",
              "Used",
            ],
            picked: new_old,
            itemBuilder: (Radio rb, Text txt, int i){
              return Column(
                children: <Widget>[
                  rb,
                  txt,
                ],
              );
            },
          ),

          const SizedBox(
            height: 10,
          ),
          Visibility(
            visible: new_old=='Used'? true: false,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                const SizedBox(
                  height: 10,
                ),
                Container(
                    margin: EdgeInsets.only(top: 10, left: 10),
                    child: Text("How much old?", textAlign: TextAlign.left,)
                ),
                Row(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(left: 10, right: 10),
                      width: 100,
                      child: new TextFormField(
                        style: TextStyle(color: Theme.of(context).accentColor),
                        keyboardType: TextInputType.number,
                        maxLength: 2,
                        initialValue: old_month,
                        decoration: new InputDecoration(
                          hintText: 'Month',
                          counterText: "",
                          hintStyle: Theme.of(context).textTheme.body1.merge(
                            TextStyle(color: Theme.of(context).accentColor),
                          ),
                          enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: Theme.of(context).accentColor.withOpacity(0.2))),
                          focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Theme.of(context).accentColor)),
                        ),
                        onChanged: (input){
                          setState(() {
                            old_month = input;
                          });
                        },
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left:10, right: 10),
                      width: 100,
                      child: new TextFormField(
                        style: TextStyle(color: Theme.of(context).accentColor),
                        keyboardType: TextInputType.number,
                        maxLength: 4,
                        initialValue: old_year,
                        decoration: new InputDecoration(
                          hintText: 'Year',
                          counterText: "",
                          hintStyle: Theme.of(context).textTheme.body1.merge(
                            TextStyle(color: Theme.of(context).accentColor),
                          ),
                          enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: Theme.of(context).accentColor.withOpacity(0.2))),
                          focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Theme.of(context).accentColor)),
                        ),
                        onChanged: (input){
                          setState(() {
                            old_year = input;
                          });
                        },
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 15,
                ),
                Container(
                    margin: EdgeInsets.only(left: 10),
                    child: FutureBuilder(
                      future: getConditionList(),
                      builder: (context,res){

                        if (!res.hasData) {
                          return Center(child: CircularProgressIndicator());
                        } else {

                          print("Response44: "+res.data.toString());

                          ConditionResponse conditionResponse = ConditionResponse.fromJson(res.data);

                          conditionList.clear();

                          for(int i=0;i<conditionResponse.condition.length;i++){
                            conditionNameIdMap.putIfAbsent(conditionResponse.condition[i].name, ()=> conditionResponse.condition[i].id);
                            conditionList.add(conditionResponse.condition[i].name);
                          }

                          print("conditionNameIdMap: "+conditionResponse.toString());

                          print("conditionNameIdMap: "+conditionNameIdMap.toString());

                          return DropdownButtonHideUnderline(
                              child: DropdownButton<String>(
                                value: condition,
                                hint: Text("Condition"),
                                icon: Icon(Icons.keyboard_arrow_down),
                                iconSize: 24,
                                onChanged: (String newValue) {
                                  setState(() {
                                    condition = newValue;
                                  });
                                },
                                items: conditionList.map<DropdownMenuItem<String>>((String value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Text(value),
                                  );
                                }).toList(),
                              )
                          );
                        }

                      },
                    )
                ),
                Container(
                    margin: EdgeInsets.only(top: 10, left: 10),
                    child: Text("With case?", textAlign: TextAlign.left,)
                ),
                RadioButtonGroup(
                  orientation: GroupedButtonsOrientation.HORIZONTAL,
                  margin: const EdgeInsets.only(left: 0.0),
                  onSelected: (String selected) => setState((){
                    isWithCase = selected;
                  }),
                  labels: <String>[
                    "Yes",
                    "No",
                  ],
                  picked: isWithCase,
                  itemBuilder: (Radio rb, Text txt, int i){
                    return Column(
                      children: <Widget>[
                        rb,
                        txt,
                      ],
                    );
                  },
                ),
                const SizedBox(
                  height: 10,
                ),
                Container(
                    margin: EdgeInsets.only(top: 10, left: 10),
                    child: Text("Under warranty?", textAlign: TextAlign.left,)
                ),
                RadioButtonGroup(
                  orientation: GroupedButtonsOrientation.HORIZONTAL,
                  margin: const EdgeInsets.only(left: 0.0),
                  onSelected: (String selected) => setState((){
                    isUnderWarranty = selected;
                  }),
                  labels: <String>[
                    "Yes",
                    "No",
                  ],
                  picked: isUnderWarranty,
                  itemBuilder: (Radio rb, Text txt, int i){
                    return Column(
                      children: <Widget>[
                        rb,
                        txt,
                      ],
                    );
                  },
                ),
                const SizedBox(
                  height: 10,
                ),
                Visibility(
                  visible: isUnderWarranty=='Yes'? true: false,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Container(
                          margin: EdgeInsets.only(top: 10, left: 10),
                          child: Text("How much time is remaining?", textAlign: TextAlign.left,)
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 10, right: 10),
                        width: 100,
                        child: new TextFormField(
                          style: TextStyle(color: Theme.of(context).accentColor),
                          keyboardType: TextInputType.number,
                          maxLength: 2,
                          initialValue: howMuchTimeRemaining,
                          decoration: new InputDecoration(
                            hintText: 'Months',
                            counterText: "",
                            hintStyle: Theme.of(context).textTheme.body1.merge(
                              TextStyle(color: Theme.of(context).accentColor),
                            ),
                            enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: Theme.of(context).accentColor.withOpacity(0.2))),
                            focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Theme.of(context).accentColor)),
                          ),
                          onChanged: (input){
                            setState(() {
                              howMuchTimeRemaining = input;
                            });
                          },
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Container(
                    margin: EdgeInsets.only(top: 10, left: 10),
                    child: Text("Original receipt?", textAlign: TextAlign.left,)
                ),
                RadioButtonGroup(
                  orientation: GroupedButtonsOrientation.HORIZONTAL,
                  margin: const EdgeInsets.only(left: 0.0),
                  onSelected: (String selected) => setState((){
                    isOriginalRceipt = selected;
                  }),
                  labels: <String>[
                    "Yes",
                    "No",
                  ],
                  picked: isOriginalRceipt,
                  itemBuilder: (Radio rb, Text txt, int i){
                    return Column(
                      children: <Widget>[
                        rb,
                        txt,
                      ],
                    );
                  },
                ),
              ],
            ),
          ),

          const SizedBox(
            height: 10,
          ),
          Container(
              margin: EdgeInsets.only(left: 10),
              child: FutureBuilder(
                future: getMaterialList(),
                builder: (context,res){

                  if (!res.hasData) {
                    return Center(child: CircularProgressIndicator());
                  } else {

                    print("Response33: "+res.data.toString());

                    MaterialResponse materialResponse = MaterialResponse.fromJson(res.data);

                    materialList.clear();

                    for(int i=0;i<materialResponse.condition.length;i++){
                      materialNameIdMap.putIfAbsent(materialResponse.condition[i].name, ()=> materialResponse.condition[i].id);
                      materialList.add(materialResponse.condition[i].name);
                    }

                    print("materialResponse: "+materialResponse.toString());

                    print("materialNameIdMap: "+materialNameIdMap.toString());

                    return DropdownButtonHideUnderline(
                        child: DropdownButton<String>(
                          value: material,
                          hint: Text("Material"),
                          icon: Icon(Icons.keyboard_arrow_down),
                          iconSize: 24,
                          onChanged: (String newValue) {
                            setState(() {
                              material = newValue;
                            });
                          },
                          items: materialList.map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),

                        )
                    );
                  }

                },
              )
          ),
          Container(
            margin: EdgeInsets.only(left: 10,right: 10),
            child: new TextFormField(
              style: TextStyle(color: Theme.of(context).accentColor),
              keyboardType: TextInputType.text,
              initialValue: type,
              decoration: new InputDecoration(
                hintText: 'Type',
                hintStyle: Theme.of(context).textTheme.body1.merge(
                  TextStyle(color: Theme.of(context).accentColor),
                ),
                enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: Theme.of(context).accentColor.withOpacity(0.2))),
                focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Theme.of(context).accentColor)),
              ),
              onChanged: (input){
                setState(() {
                  type = input;
                });
              },
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            margin: EdgeInsets.only(left: 10,right: 10),
            child: new TextField(
              style: TextStyle(color: Theme.of(context).accentColor),
              keyboardType: TextInputType.text,
              decoration: new InputDecoration(
                hintText: 'Color',
                hintStyle: Theme.of(context).textTheme.body1.merge(
                  TextStyle(color: Theme.of(context).accentColor),
                ),
                enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: Theme.of(context).accentColor.withOpacity(0.2))),
                focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Theme.of(context).accentColor)),
              ),
              onChanged: (input){
                setState(() {
                  color = input;
                });
              },
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            margin: EdgeInsets.only(left: 10,right: 10),
            child: new TextField(
              style: TextStyle(color: Theme.of(context).accentColor),
              keyboardType: TextInputType.number,
              decoration: new InputDecoration(
                hintText: 'Price',
                hintStyle: Theme.of(context).textTheme.body1.merge(
                  TextStyle(color: Theme.of(context).accentColor),
                ),
                enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: Theme.of(context).accentColor.withOpacity(0.2))),
                focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Theme.of(context).accentColor)),
              ),
              onChanged: (input){
                setState(() {
                  price = input;
                });
              },
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          RadioButtonGroup(
            orientation: GroupedButtonsOrientation.HORIZONTAL,
            margin: const EdgeInsets.only(left: 0.0),
            onSelected: (String selected) => setState((){
              fixed_negotiable = selected;
            }),
            labels: <String>[
              "Fixed",
              "Negotiable",
            ],
            picked: fixed_negotiable,
            itemBuilder: (Radio rb, Text txt, int i){
              return Column(
                children: <Widget>[
                  rb,
                  txt,
                ],
              );
            },
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            margin: EdgeInsets.only(left: 10,right: 10),
            child: new TextField(
              style: TextStyle(color: Theme.of(context).accentColor),
              keyboardType: TextInputType.number,
              maxLength: 10,
              decoration: new InputDecoration(
                hintText: 'Contact Number',
                hintStyle: Theme.of(context).textTheme.body1.merge(
                  TextStyle(color: Theme.of(context).accentColor),
                ),
                enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: Theme.of(context).accentColor.withOpacity(0.2))),
                focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Theme.of(context).accentColor)),
                counterText: ''
              ),
              onChanged: (input){
                setState(() {
                  contact_number = input;
                });
              },
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            margin: EdgeInsets.only(left: 10,right: 10),
            child: new TextField(
              style: TextStyle(color: Theme.of(context).accentColor),
              keyboardType: TextInputType.number,
              maxLength: 10,
              decoration: new InputDecoration(
                hintText: 'WhatsApp Number',
                hintStyle: Theme.of(context).textTheme.body1.merge(
                  TextStyle(color: Theme.of(context).accentColor),
                ),
                enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: Theme.of(context).accentColor.withOpacity(0.2))),
                focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Theme.of(context).accentColor)),
                counterText: '',
              ),
              onChanged: (input){
                setState(() {
                  whatsapp_number = input;
                });
              },
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            margin: EdgeInsets.only(left: 10,right: 10),
            child: new TextField(
              style: TextStyle(color: Theme.of(context).accentColor),
              keyboardType: TextInputType.multiline,
              maxLines: 3,
              decoration: new InputDecoration(
                hintText: "Description",
                hintStyle: Theme.of(context).textTheme.body1.merge(
                  TextStyle(color: Theme.of(context).accentColor,),
                ),
                enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: Theme.of(context).accentColor.withOpacity(0.2))),
                focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Theme.of(context).accentColor)),
                counterText: ''
              ),
              onChanged: (input){
                setState(() {
                  description = input;
                });
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Container(
                  width: 100,
                  height: 100,
                  decoration: BoxDecoration(
                    border: Border.all(style: BorderStyle.solid, width: 1, color: Colors.grey[300]),
                  ),
                  child:_frontimage!=null? Stack(
                    alignment: Alignment.topRight,
                    children: <Widget>[
                      Center(child: Image.file(_frontimage, fit: BoxFit.fill)),
                      Container(
                        height: 33,
                        width: 33,
                        color: Colors.black87,
                        child: IconButton(
                          icon: Icon(Icons.close, color: Colors.white,),
                          color: Colors.black87,
                          splashColor: Colors.black87,
                          focusColor: Colors.black87,
                          onPressed: (){
                            setState(() {
                              _frontimage=null;
                            });
                          },
                          iconSize: 20,
                        ),
                      )
                    ],
                  ): Column(
                    children: <Widget>[
                      IconButton(
                        icon: Icon(Icons.add),
                        onPressed: (){
                          getFrontImage(context);
                        },
                      ),
                      Text('Front Image')
                    ],
                  ),

                ),
                Container(
                  width: 100,
                  height: 100,
                  decoration: BoxDecoration(
                    border: Border.all(style: BorderStyle.solid, width: 1, color: Colors.grey[300]),
                  ),
                  child:_backImage!=null? Stack(
                    alignment: Alignment.topRight,
                    children: <Widget>[
                      Center(child: Image.file(_backImage, fit: BoxFit.fill)),
                      Container(
                        height: 33,
                        width: 33,
                        color: Colors.black87,
                        child: IconButton(
                          icon: Icon(Icons.close, color: Colors.white,),
                          color: Colors.black87,
                          splashColor: Colors.black87,
                          focusColor: Colors.black87,
                          onPressed: (){
                            setState(() {
                              _backImage=null;
                            });
                          },
                          iconSize: 20,
                        ),
                      )
                    ],
                  ): Column(
                    children: <Widget>[
                      IconButton(
                        icon: Icon(Icons.add),
                        onPressed: (){
                          getBackImage(context);
                        },
                      ),
                      Text('Back Side')
                    ],
                  ),

                ),
                Container(
                  width: 100,
                  height: 100,
                  decoration: BoxDecoration(
                    border: Border.all(style: BorderStyle.solid, width: 1, color: Colors.grey[300]),
                  ),
                  child:_rightImage!=null? Stack(
                    alignment: Alignment.topRight,
                    children: <Widget>[
                      Center(child: Image.file(_rightImage, fit: BoxFit.fill)),
                      Container(
                        height: 33,
                        width: 33,
                        color: Colors.black87,
                        child: IconButton(
                          icon: Icon(Icons.close, color: Colors.white,),
                          color: Colors.black87,
                          splashColor: Colors.black87,
                          focusColor: Colors.black87,
                          onPressed: (){
                            setState(() {
                              _rightImage=null;
                            });
                          },
                          iconSize: 20,
                        ),
                      )
                    ],
                  ) : Column(
                    children: <Widget>[
                      IconButton(
                        icon: Icon(Icons.add),
                        onPressed: (){
                          getRightImage(context);
                        },
                      ),
                      Text('Right Side')
                    ],
                  ),

                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Container(
                  width: 100,
                  height: 100,
                  decoration: BoxDecoration(
                    border: Border.all(style: BorderStyle.solid, width: 1, color: Colors.grey[300]),
                  ),
                  child:_leftImage!=null? Stack(
                    alignment: Alignment.topRight,
                    children: <Widget>[
                      Center(child: Image.file(_leftImage, fit: BoxFit.fill)),
                      Container(
                        height: 33,
                        width: 33,
                        color: Colors.black87,
                        child: IconButton(
                          icon: Icon(Icons.close, color: Colors.white,),
                          color: Colors.black87,
                          splashColor: Colors.black87,
                          focusColor: Colors.black87,
                          onPressed: (){
                            setState(() {
                              _leftImage=null;
                            });
                          },
                          iconSize: 20,
                        ),
                      )
                    ],
                  ): Column(
                    children: <Widget>[
                      IconButton(
                        icon: Icon(Icons.add),
                        onPressed: (){
                          getLeftImage(context);
                        },
                      ),
                      Text('Left Side')
                    ],
                  ),

                ),
                Container(
                  width: 100,
                  height: 100,
                  decoration: BoxDecoration(
                    border: Border.all(style: BorderStyle.solid, width: 1, color: Colors.grey[300]),
                  ),
                  child:_boxAndPaperImage!=null? Stack(
                    alignment: Alignment.topRight,
                    children: <Widget>[
                      Center(child: Image.file(_boxAndPaperImage, fit: BoxFit.fill)),
                      Container(
                        height: 33,
                        width: 33,
                        color: Colors.black87,
                        child: IconButton(
                          icon: Icon(Icons.close, color: Colors.white,),
                          color: Colors.black87,
                          splashColor: Colors.black87,
                          focusColor: Colors.black87,
                          onPressed: (){
                            setState(() {
                              _boxAndPaperImage=null;
                            });
                          },
                          iconSize: 20,
                        ),
                      )
                    ],
                  ): Column(
                    children: <Widget>[
                      IconButton(
                        icon: Icon(Icons.add),
                        onPressed: (){
                          getBoxAndPaperImage(context);
                        },
                      ),
                      Text('Box and Paper')
                    ],
                  ),

                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: RaisedButton(
              color: Theme.of(context).buttonColor,
              child: Text('POST AD'),
              onPressed: (){
                // save ad details to database

                if(category.isEmpty){
                  showInDialog(context, 'Empty Field', 'Select Category');
                }
                else if(brand.isEmpty && brand_name.isEmpty){
                  category=='Watches'? showInDialog(context, 'Empty Field', 'Select Brand'): showInDialog(context, 'Empty Field', 'Enter Brand');
                }
                else if(model_name.isEmpty){
                  showInDialog(context, 'Empty Field', 'Enter Model Name');
                }
                else if(material.isEmpty){
                  showInDialog(context, 'Empty Field', 'Enter Material');
                }
                else if(type.isEmpty){
                  showInDialog(context, 'Empty Field', 'Enter Type');
                }
                else if(color.isEmpty){
                  showInDialog(context, 'Empty Field', 'Enter Color');
                }
                else if(price.isEmpty){
                  showInDialog(context, 'Empty Field', 'Enter Price');
                }
                else if(contact_number.isEmpty){
                  showInDialog(context, 'Empty Field', 'Enter Contact Number');
                }
                else if(whatsapp_number.isEmpty){
                  showInDialog(context, 'Empty Field', 'Enter WhatsApp Number');
                }
                else if(description.isEmpty){
                  showInDialog(context, 'Empty Field', 'Enter Description');
                }
                else if(_frontimage==null && _backImage==null && _rightImage==null && _leftImage==null && _boxAndPaperImage==null)
                {
                    showInDialog(context, 'Empty Field', 'Select Atleast One Image');
                }else{

                  String brand_name1;

                  if(categoryNameIdMap[category].toString().toLowerCase()=='watches'){
                    brand_name1 = brandNameIdMap[brand];
                  }else{
                    brand_name1 = brand_name1;
                  }

                  _postAd(categoryNameIdMap[category], brand, brand_name, model_name, new_old, material, type, color, price, fixed_negotiable, contact_number, whatsapp_number, description, old_month, old_year, condition, isWithCase, isUnderWarranty, howMuchTimeRemaining, isOriginalRceipt, _frontimage, _backImage, _rightImage, _leftImage, _boxAndPaperImage).whenComplete((){
                    showInDialog(context, 'Response', 'Posted');
                  }).catchError((error){
                    showInDialog(context, 'Error', error.toString());
                  });

                }

              }
            )
          ),

        ],
      ),
    );
  }



  Future<dynamic> _postAd(String category, String brand, String brand_name, String model_name, String new_old, String material, String type, String color, String price, String fixed_negotiable, String contact_number, String whatsapp_number, String description, String old_month, String old_year, String condition, String isWithCase, String isUnderWarranty, String howMuchTimeRemaining, String isOriginalRceipt, File _frontimage, File _backImage, File _rightImage, File _leftImage, File _boxAndPaperImage) async {
    try {
      Options options = Options(
        contentType: ContentType.parse('application/json'),
      );

      FormData formdata = new FormData(); // just like JS
      if(_frontimage!=null) {
        formdata.add("img_1", new UploadFileInfo(_frontimage, _frontimage.path.split('/').last));
      }
      if(_backImage!=null) {
        formdata.add("img_2", new UploadFileInfo(_backImage, _backImage.path.split('/').last));
      }
      if(_rightImage!=null) {
        formdata.add("img_3", new UploadFileInfo(_rightImage, _rightImage.path.split('/').last));
      }
      if(_leftImage!=null){
        formdata.add("img_4", new UploadFileInfo(_leftImage, _leftImage.path.split('/').last));
      }
      if(_boxAndPaperImage!=null){
        formdata.add("boxAndPaperImage", new UploadFileInfo(_boxAndPaperImage, _boxAndPaperImage.path.split('/').last));
      }
      formdata.add("category", category);
      formdata.add("user_id", userid);
      formdata.add("brand", brand);
      formdata.add("brand_name", brand_name);
      formdata.add("model_name", model_name);
      formdata.add("material", material);
      formdata.add("type", type);
      formdata.add("color", color);
      formdata.add("price", price);
      formdata.add("fixed_negotiable", fixed_negotiable);
      formdata.add("contact_number", contact_number);
      formdata.add("whatsapp_number", whatsapp_number);
      formdata.add("description", description);
      formdata.add("old_month", old_month);
      formdata.add("old_year", old_year);
      formdata.add("condition", condition);
      formdata.add("isUnderWarranty", isUnderWarranty);
      formdata.add("howmuchtimeremaining", howMuchTimeRemaining);
      formdata.add("isOriginalRceipt", isOriginalRceipt);
      formdata.add("old_year", old_year);

      var response = await dio.post('adpost.php', data: formdata , options: options);

      if (response.statusCode == 200 || response.statusCode == 201) {
        var responseJson = json.decode(response.data);
        return responseJson;
      } else
        showInDialog(context, "Registration Error", 'Error: '+response.statusCode.toString());
      print('responseCode: '+ response.statusCode.toString());
    } on DioError catch (exception) {
      if (exception == null || exception.toString().contains('SocketException')) {
        showInDialog(context, "Error", "Network Error");
      } else if (exception.type == DioErrorType.RECEIVE_TIMEOUT ||
          exception.type == DioErrorType.CONNECT_TIMEOUT) {
        showInDialog(
            context,
            "Error",
            "Could'nt connect, please ensure you have a stable network.");
      } else {
        return null;
      }
    }
  }

  Future<void> getCategoryList() async {
    try {
      Options options = Options(
        contentType: ContentType.parse('application/json'),
      );

      var response = await dio.get('category.php', options: options);

      if (response.statusCode == 200 || response.statusCode == 201) {
        var responseJson = json.decode(response.data);

        print("Response: "+responseJson.toString());

        return responseJson;
      } else
        print("Error: "+response.statusCode.toString());
    } on DioError catch (exception) {
      if (exception == null ||
          exception.toString().contains('SocketException')) {
        print("Error: "+"Network Error");
      } else if (exception.type == DioErrorType.RECEIVE_TIMEOUT ||
          exception.type == DioErrorType.CONNECT_TIMEOUT) {
        print("Error: "+ "Could'nt connect, please ensure you have a stable network.");
      } else {
        return null;
      }
    }
  }

  Future<void> getBrandList() async {
    try {
      Options options = Options(
        contentType: ContentType.parse('application/json'),
      );

      var response = await dio.get('brand.php', options: options);

      if (response.statusCode == 200 || response.statusCode == 201) {
        var responseJson = json.decode(response.data);

        print("Response: "+responseJson.toString());

        return responseJson;
      } else
        print("Error: "+response.statusCode.toString());
    } on DioError catch (exception) {
      if (exception == null ||
          exception.toString().contains('SocketException')) {
        print("Error: "+"Network Error");
      } else if (exception.type == DioErrorType.RECEIVE_TIMEOUT ||
          exception.type == DioErrorType.CONNECT_TIMEOUT) {
        print("Error: "+ "Could'nt connect, please ensure you have a stable network.");
      } else {
        return null;
      }
    }
  }

  Future<void> getConditionList() async {
    try {
      Options options = Options(
        contentType: ContentType.parse('application/json'),
      );

      var response = await dio.get('condition.php', options: options);

      if (response.statusCode == 200 || response.statusCode == 201) {
        var responseJson = json.decode(response.data);

        print("Response: "+responseJson.toString());

        return responseJson;
      } else
        print("Error: "+response.statusCode.toString());
    } on DioError catch (exception) {
      if (exception == null ||
          exception.toString().contains('SocketException')) {
        print("Error: "+"Network Error");
      } else if (exception.type == DioErrorType.RECEIVE_TIMEOUT ||
          exception.type == DioErrorType.CONNECT_TIMEOUT) {
        print("Error: "+ "Could'nt connect, please ensure you have a stable network.");
      } else {
        return null;
      }
    }
  }

  Future<void> getMaterialList() async {
    try {
      Options options = Options(
        contentType: ContentType.parse('application/json'),
      );

      var response = await dio.get('material.php', options: options);

      if (response.statusCode == 200 || response.statusCode == 201) {
        var responseJson = json.decode(response.data);

        print("Response: "+responseJson.toString());

        return responseJson;
      } else
        print("Error: "+response.statusCode.toString());
    } on DioError catch (exception) {
      if (exception == null ||
          exception.toString().contains('SocketException')) {
        print("Error: "+"Network Error");
      } else if (exception.type == DioErrorType.RECEIVE_TIMEOUT ||
          exception.type == DioErrorType.CONNECT_TIMEOUT) {
        print("Error: "+ "Could'nt connect, please ensure you have a stable network.");
      } else {
        return null;
      }
    }
  }

  void showInDialog(BuildContext context, String title, String value) async {
    showDialog(
      context: context,
      child: new AlertDialog(
        title: Text(title),
        content: Text(value),
        actions: [
          new FlatButton(
            child: const Text("Ok"),
            onPressed: () => Navigator.pop(context),
          ),
        ],
      ),
    );
  }

}
