import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:ecommerce_app_ui_kit/src/models/brand.dart';
import 'package:ecommerce_app_ui_kit/src/models/category.dart';
import 'package:ecommerce_app_ui_kit/src/responses/BrandResponse.dart';
import 'package:ecommerce_app_ui_kit/src/widgets/BrandGridWidget.dart';
import 'package:ecommerce_app_ui_kit/src/widgets/DrawerWidget.dart';
import 'package:ecommerce_app_ui_kit/src/widgets/SearchBarWidget.dart';
import 'package:ecommerce_app_ui_kit/src/widgets/ShoppingCartButtonWidget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:flutter_svg/svg.dart';

class BrandsWidget extends StatefulWidget {
  @override
  _BrandsWidgetState createState() => _BrandsWidgetState();
}

class _BrandsWidgetState extends State<BrandsWidget> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
//  BrandsList _brandsList = new BrandsList();
  SubCategoriesList _subCategoriesList = new SubCategoriesList();

  static var uri = "http://qmovement.online/webservices/";

  static BaseOptions options = BaseOptions(
      baseUrl: uri,
      responseType: ResponseType.plain,
      connectTimeout: 30000,
      receiveTimeout: 30000,
      validateStatus: (code) {
        if (code >= 200) {
          return true;
        }
        else{
          return false;
        }
      });

  static Dio dio = Dio(options);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      drawer: DrawerWidget(),
      appBar: AppBar(
        automaticallyImplyLeading: false,
        leading: new IconButton(
          icon: new Icon(Icons.sort, color: Theme.of(context).hintColor),
          onPressed: () => _scaffoldKey.currentState.openDrawer(),
        ),
        backgroundColor: Colors.transparent,
        elevation: 0,
        title: Text(
          'Brands',
          style: Theme.of(context).textTheme.display1,
        ),
        actions: <Widget>[
          Container(
              width: 30,
              height: 30,
              margin: EdgeInsets.only(top: 12.5, bottom: 12.5, right: 20),
              child: InkWell(
                borderRadius: BorderRadius.circular(300),
                onTap: () {
                  Navigator.of(context).pushNamed('/Tabs', arguments: 1);
                },
                child: CircleAvatar(
                  backgroundImage: AssetImage('img/user2.jpg'),
                ),
              )),
        ],
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        child: Wrap(
          children: <Widget>[
            FutureBuilder(
              future: getBrandsList(),
              builder: (context,res){

                if (!res.hasData) {
                  return Center(child: CircularProgressIndicator());
                } else {

                  print("Response2: "+res.data.toString());

                  BrandResponse brandResponse = BrandResponse.fromJson(res.data);

                  brandResponse.categories.length;

                  return StaggeredGridView.countBuilder(
                    primary: false,
                    shrinkWrap: true,
                    padding: EdgeInsets.only(top: 15),
                    crossAxisCount: MediaQuery.of(context).orientation == Orientation.portrait ? 2 : 4,
                    itemCount: brandResponse.categories.length,
                    itemBuilder: (BuildContext context, int index) {
                      return InkWell(
                        onTap: () {
//                          Navigator.of(context)
//                              .pushNamed('/Brand', arguments: new RouteArgument(id: brand.id, argumentsList: [brand]));
                        },
                        child: Stack(
                          alignment: AlignmentDirectional.topCenter,
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.all(10),
                              alignment: AlignmentDirectional.topCenter,
                              padding: EdgeInsets.all(20),
                              width: double.infinity,
                              height: 100,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  boxShadow: [
                                    BoxShadow(
                                        color: Theme.of(context).hintColor.withOpacity(0.10), offset: Offset(0, 4), blurRadius: 10)
                                  ],
                                  gradient: LinearGradient(begin: Alignment.bottomLeft, end: Alignment.topRight, colors: [
                                    Colors.green,
                                    Colors.greenAccent
                                  ])),
                              child: Hero(
                                tag: brandResponse.categories[index].id,
                                child: Image.network(
                                  brandResponse.categories[index].image==null?'http':brandResponse.categories[index].image,
                                  color: Theme.of(context).primaryColor,
                                  width: 80,
                                ),
                              ),
                            ),
                            Positioned(
                              right: -50,
                              bottom: -100,
                              child: Container(
                                width: 220,
                                height: 220,
                                decoration: BoxDecoration(
                                  color: Theme.of(context).primaryColor.withOpacity(0.08),
                                  borderRadius: BorderRadius.circular(150),
                                ),
                              ),
                            ),
                            Positioned(
                              left: -30,
                              top: -60,
                              child: Container(
                                width: 120,
                                height: 120,
                                decoration: BoxDecoration(
                                  color: Theme.of(context).primaryColor.withOpacity(0.12),
                                  borderRadius: BorderRadius.circular(150),
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 80, bottom: 10),
                              padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
                              width: 140,
                              height: 80,
                              decoration: BoxDecoration(
                                  color: Theme.of(context).primaryColor,
                                  borderRadius: BorderRadius.circular(6),
                                  boxShadow: [
                                    BoxShadow(
                                        color: Theme.of(context).hintColor.withOpacity(0.15), offset: Offset(0, 3), blurRadius: 10)
                                  ]),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    brandResponse.categories[index].nameEng==null? ' ': brandResponse.categories[index].nameEng,
                                    style: Theme.of(context).textTheme.body2,
                                    maxLines: 1,
                                    softWrap: false,
                                    overflow: TextOverflow.fade,
                                  ),
                                  Row(
                                    children: <Widget>[
                                      // The title of the product
                                      Expanded(
                                        child: Text(
                                          //'${brand.products.length} Products',
                                          '0 Products',
                                          style: Theme.of(context).textTheme.body1,
                                          overflow: TextOverflow.fade,
                                          softWrap: false,
                                        ),
                                      ),
                                    ],
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      );
                    },
//                  staggeredTileBuilder: (int index) => new StaggeredTile.fit(index % 2 == 0 ? 1 : 2),
                    staggeredTileBuilder: (int index) => new StaggeredTile.fit(1),
                    mainAxisSpacing: 15.0,
                    crossAxisSpacing: 15.0,
                  );
                }
              },
            )
          ],
        ),
      ),
    );
  }


  Future<void> getBrandsList() async {
    try {
      Options options = Options(
        contentType: ContentType.parse('application/json'),
      );

      var response = await dio.get('brand.php', options: options);

      if (response.statusCode == 200 || response.statusCode == 201) {
        var responseJson = json.decode(response.data);

        print("Response: "+responseJson.toString());

        return responseJson;
      } else
        print("Error: "+response.statusCode.toString());
    } on DioError catch (exception) {
      if (exception == null ||
          exception.toString().contains('SocketException')) {
        print("Error: "+"Network Error");
      } else if (exception.type == DioErrorType.RECEIVE_TIMEOUT ||
          exception.type == DioErrorType.CONNECT_TIMEOUT) {
        print("Error: "+ "Could'nt connect, please ensure you have a stable network.");
      } else {
        return null;
      }
    }
  }

}
