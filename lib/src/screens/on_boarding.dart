import 'dart:io';

import'package:carousel_slider/carousel_slider.dart';
import 'package:dio/dio.dart';
import 'package:ecommerce_app_ui_kit/config/app_config.dart' as config;
import 'package:ecommerce_app_ui_kit/src/responses/IntroBanner.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class OnBoardingWidget extends StatefulWidget {
  @override
  _OnBoardingWidgetState createState() => _OnBoardingWidgetState();
}

class _OnBoardingWidgetState extends State<OnBoardingWidget> {
  int _current = 0;

  static var uri = "http://qmovement.online/webservices/";

  static BaseOptions options = BaseOptions(
      baseUrl: uri,
      responseType: ResponseType.plain,
      connectTimeout: 30000,
      receiveTimeout: 30000,
      validateStatus: (code) {
        if (code >= 200) {
          return true;
        }
        else{
          return false;
        }
      });

  static Dio dio = Dio(options);

  IntroBanner introBanner;


  @override
  void initState() {

    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(right: 20, top: 50),
//              child: FlatButton(
//                onPressed: () {
//                  Navigator.of(context).pushNamed('/Tabs', arguments: 2);
//                },
//                child: Text(
//                  'Skip',
//                  style: Theme.of(context).textTheme.button,
//                ),
//                color: Theme.of(context).accentColor,
//                shape: StadiumBorder(),
//              ),
            ),

            FutureBuilder(
              future: getIntroBanner(),
              builder: (context,res){

                if (!res.hasData) {

                  return CarouselSlider(
                      height: 500.0,
                      viewportFraction: 1.0,
                      onPageChanged: (index) {
                        setState(() {
                          _current = index;
                        });
                        },
                      items: [
                        Center(child: CircularProgressIndicator())
                      ]
                  );
                } else {

                  print("Response2: "+res.data.toString());

                  introBanner = IntroBanner.fromJson(res.data);

                  return CarouselSlider(
                    height: 500.0,
                    viewportFraction: 1.0,
                    onPageChanged: (index) {
                      setState(() {
                        _current = index;
                      });
                    },
                    items: introBanner.banner.map((boarding) {
                      print(boarding[_current].toString());
                      return Builder(
                        builder: (BuildContext context) {
                          return Column(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: <Widget>[
                              Padding(
                                  padding: const EdgeInsets.all(20),
                                  child: Image.network(boarding, width: 500,)
                              ),
//                        Container(
//                          width: config.App(context).appWidth(75),
//                          padding: const EdgeInsets.only(right: 20),
//                          child: Text(
//                            _onBoardingList.list[0].description,
//                            style: Theme.of(context).textTheme.display1,
//                          ),
//                        ),
                            ],
                          );
                        },
                      );
                    }).toList(),
                  );

                }

              },
            ),

            FutureBuilder(
              future: getIntroBanner(),
              builder: (context,res){


                if (!res.hasData) {
                  return  Center(child: CircularProgressIndicator());
                } else {

                  print("Response2: "+res.data.toString());

                  introBanner = IntroBanner.fromJson(res.data);

                  return Container(
                    width: config.App(context).appWidth(75),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: introBanner.banner.map((boarding) {
                        return Container(
                          width: 25.0,
                          height: 3.0,
                          margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(
                                Radius.circular(8),
                              ),
                              color: _current == introBanner.banner.indexOf(boarding)
                                  ? Theme.of(context).hintColor.withOpacity(0.8)
                                  : Theme.of(context).hintColor.withOpacity(0.2)),
                        );
                      }).toList(),
                    ),
                  );
                }

              },
            ),

            Container(
              width: config.App(context).appWidth(75),
              padding: const EdgeInsets.symmetric(vertical: 50),
              child: FlatButton(
                padding: EdgeInsets.symmetric(horizontal: 35, vertical: 12),
                onPressed: () {
                  Navigator.of(context).pushNamed('/SignIn');
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      'Sign In',
                      style: Theme.of(context).textTheme.display1.merge(
                            TextStyle(color: Theme.of(context).primaryColor),
                          ),
                    ),
                    Icon(
                      Icons.arrow_forward,
                      color: Theme.of(context).primaryColor,
                    ),
                  ],
                ),
                color: Theme.of(context).accentColor,
                shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.only(
                    topLeft: Radius.circular(50),
                    bottomLeft: Radius.circular(50),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> getIntroBanner() async {
    try {
      Options options = Options(
        contentType: ContentType.parse('application/json'),
      );

      var response = await dio.get('introbanner.php', options: options);

      if (response.statusCode == 200 || response.statusCode == 201) {
        var responseJson = json.decode(response.data);

        print("Response: "+responseJson.toString());

        return responseJson;
      } else
        print("Error: "+response.statusCode.toString());
    } on DioError catch (exception) {
      if (exception == null ||
          exception.toString().contains('SocketException')) {
        print("Error: "+"Network Error");
      } else if (exception.type == DioErrorType.RECEIVE_TIMEOUT ||
          exception.type == DioErrorType.CONNECT_TIMEOUT) {
        print("Error: "+ "Could'nt connect, please ensure you have a stable network.");
      } else {
        return null;
      }
    }
  }

}
