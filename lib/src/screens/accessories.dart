import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:ecommerce_app_ui_kit/src/models/brand.dart';
import 'package:ecommerce_app_ui_kit/src/models/category.dart';
import 'package:ecommerce_app_ui_kit/src/models/product.dart';
import 'package:ecommerce_app_ui_kit/src/responses/CategoryResponse.dart';
import 'package:ecommerce_app_ui_kit/src/responses/ProductResponse.dart';
import 'package:ecommerce_app_ui_kit/src/widgets/CategoriesIconsCarouselWidget.dart';
import 'package:ecommerce_app_ui_kit/src/widgets/CategorizedProductsWidget.dart';
import 'package:ecommerce_app_ui_kit/src/widgets/DrawerWidget.dart';
import 'package:flutter/material.dart';
import 'package:sticky_headers/sticky_headers.dart';

class AccessoriesWidget extends StatefulWidget {
  @override
  _AccessoriesWidgetState createState() => _AccessoriesWidgetState();
}

class _AccessoriesWidgetState extends State<AccessoriesWidget> with SingleTickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  List<Product> _productsOfCategoryList;
  CategoriesList _categoriesList = new CategoriesList();
  BrandsList _brandsList = new BrandsList();
  ProductsList _productsList = new ProductsList();

  Animation animationOpacity;
  AnimationController animationController;

  static var uri = "http://qmovement.online/webservices/";

  static BaseOptions options = BaseOptions(
      baseUrl: uri,
      responseType: ResponseType.plain,
      connectTimeout: 30000,
      receiveTimeout: 30000,
      validateStatus: (code) {
        if (code >= 200) {
          return true;
        }
        else{
          return false;
        }
      });

  static Dio dio = Dio(options);

  @override
  void initState() {
    animationController = AnimationController(duration: Duration(milliseconds: 200), vsync: this);
    CurvedAnimation curve = CurvedAnimation(parent: animationController, curve: Curves.easeIn);
    animationOpacity = Tween(begin: 0.0, end: 1.0).animate(curve)
      ..addListener(() {
        setState(() {});
      });

    animationController.forward();

    _productsOfCategoryList = _categoriesList.list.firstWhere((category) {
      return category.selected;
    }).products;

    _productsOfCategoryList.clear();

    getProductsByCategoryId(5);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      drawer: DrawerWidget(),
      appBar: AppBar(
        automaticallyImplyLeading: false,
        leading: new IconButton(
          icon: new Icon(Icons.sort, color: Theme.of(context).hintColor),
          onPressed: () => _scaffoldKey.currentState.openDrawer(),
        ),
        backgroundColor: Colors.transparent,
        elevation: 0,
        title: Text(
          'Accessories',
          style: Theme.of(context).textTheme.display1,
        ),
        actions: <Widget>[
          Container(
              width: 30,
              height: 30,
              margin: EdgeInsets.only(top: 12.5, bottom: 12.5, right: 20),
              child: InkWell(
                borderRadius: BorderRadius.circular(300),
                onTap: () {
                  Navigator.of(context).pushNamed('/Tabs', arguments: 1);
                },
                child: CircleAvatar(
                  backgroundImage: AssetImage('img/user2.jpg'),
                ),
              )),
        ],
      ),
      body: ListView(
        children: <Widget>[
          FutureBuilder(
            future: getCategoryList(),
            builder: (context,res){

              if (!res.hasData) {
                return Center(child: CircularProgressIndicator());
              } else {

                print("Response2: "+res.data.toString());

                CategoryResponse categoryResponse = CategoryResponse.fromJson(res.data);

                _categoriesList.list.clear();

                for(int i=0;i<categoryResponse.categories.length;i++) {
                  _categoriesList.list.add(
                      new Categry(categoryResponse.categories[i].id.toString(),categoryResponse.categories[i].name.toString(),categoryResponse.categories[i].picture.toString(),true,[])
                  );
                }

                return StickyHeader(
                  header: CategoriesIconsCarouselWidget(
                      heroTag: 'home_categories_1',
                      categoriesList: _categoriesList,
                      onChanged: (id) {
                        setState(() {
                          animationController.reverse().then((f) {

                            print('selectedCategoryId:'+id);

                            getProductsByCategoryId(id);

                            animationController.forward();
                          });
                        });
                      }),

                  content: CategorizedProductsWidget(animationOpacity: animationOpacity, productsList: _productsOfCategoryList, from_page: 'Accessories',),
                );
              }

            },
          ),
        ],
      ),
    );
//      ],
//    );
  }

  Future<void> getBrandsList() async {
    try {
      Options options = Options(
        contentType: ContentType.parse('application/json'),
      );

      var response = await dio.get('brand.php', options: options);

      if (response.statusCode == 200 || response.statusCode == 201) {
        var responseJson = json.decode(response.data);

        print("Response: "+responseJson.toString());

        return responseJson;
      } else
        print("Error: "+response.statusCode.toString());
    } on DioError catch (exception) {
      if (exception == null ||
          exception.toString().contains('SocketException')) {
        print("Error: "+"Network Error");
      } else if (exception.type == DioErrorType.RECEIVE_TIMEOUT ||
          exception.type == DioErrorType.CONNECT_TIMEOUT) {
        print("Error: "+ "Could'nt connect, please ensure you have a stable network.");
      } else {
        return null;
      }
    }
  }

  Future<dynamic> getProductsListByCategory(String brand_id) async {
    try {
      Options options = Options(
        contentType: ContentType.parse('application/json'),
      );

      var response = await dio.post('productbybrand.php', data: {"id":brand_id}, options: options);

      if (response.statusCode == 200 || response.statusCode == 201) {
        var responseJson = json.decode(response.data);

        print("Response: "+responseJson.toString());

        return responseJson;
      } else
        print("Error: "+response.statusCode.toString());
    } on DioError catch (exception) {
      if (exception == null ||
          exception.toString().contains('SocketException')) {
        print("Error: "+"Network Error");
      } else if (exception.type == DioErrorType.RECEIVE_TIMEOUT ||
          exception.type == DioErrorType.CONNECT_TIMEOUT) {
        print("Error: "+ "Could'nt connect, please ensure you have a stable network.");
      } else {
        return null;
      }
    }
  }

  getProductsByCategoryId(id) async {
    var res = await getProductsListByCategory(id);
    ProductResponse productResponse = ProductResponse.fromJson(res);

    print('productsByCategory:'+res.toString());
    print('productResponse:'+productResponse.categories[0].id);


    _productsOfCategoryList.clear();

    for(int i=0;i<productResponse.categories.length;i++){

//      String name = productResponse.categories[i].modelName.toString()==null? 'NA' : productResponse.categories[i].modelName.toString().isEmpty? 'NA': productResponse.categories[i].modelName.toString();
//      String image_url = productResponse.categories[i].img1.toString()==null? 'NA' : productResponse.categories[i].img1.toString().isEmpty? 'NA': productResponse.categories[i].img1.toString();
//      String modal_name = productResponse.categories[i].modelName.toString()==null? 'NA' : productResponse.categories[i].modelName.toString().isEmpty? 'NA': productResponse.categories[i].modelName.toString();
//      String condition = productResponse.categories[i].condition.toString()==null? 'NA' : productResponse.categories[i].condition.toString().isEmpty? 'NA': productResponse.categories[i].condition.toString();
//      String available = productResponse.categories[i].contactNumber.toString()==null? '0' : productResponse.categories[i].contactNumber.toString().isEmpty? '0': productResponse.categories[i].contactNumber.toString();
//      String price = productResponse.categories[i].price.toString()==null? '0.0' : productResponse.categories[i].price.toString().isEmpty? '0.0': productResponse.categories[i].price.toString();
//      String quantity = productResponse.categories[i].contactNumber.toString()==null? '0' : productResponse.categories[i].contactNumber.toString().isEmpty? '0': productResponse.categories[i].contactNumber.toString();
//      String sales = productResponse.categories[i].contactNumber.toString()==null? '0' : productResponse.categories[i].contactNumber.toString().isEmpty? '0': productResponse.categories[i].contactNumber.toString();
//      String rate = productResponse.categories[i].contactNumber.toString()==null? '0.0' : productResponse.categories[i].contactNumber.toString().isEmpty? '0.0': productResponse.categories[i].contactNumber.toString();
//      String discount = productResponse.categories[i].contactNumber.toString()==null? '0.0' : productResponse.categories[i].contactNumber.toString().isEmpty? '0.0': productResponse.categories[i].contactNumber.toString();

      _productsOfCategoryList.add(
          new Product(
            productResponse.categories[i].id.toString()?? 'NA',
            productResponse.categories[i].title.toString()?? 'NA',
            productResponse.categories[i].slug.toString()?? 'NA',
            productResponse.categories[i].category.toString()?? 'NA',
            productResponse.categories[i].subcategory.toString()?? 'NA',
            productResponse.categories[i].isOriginalRceipt.toString()?? 'NA',
            productResponse.categories[i].isUnderWarranty.toString()?? 'NA',
            productResponse.categories[i].isWithCase.toString()?? 'NA',
            productResponse.categories[i].condition.toString()?? 'NA',
            productResponse.categories[i].oldYear.toString()?? 'NA',
            productResponse.categories[i].oldMonth.toString()?? 'NA',
            productResponse.categories[i].whatsappNumber.toString()?? 'NA',
            productResponse.categories[i].contactNumber.toString()?? 'NA',
            productResponse.categories[i].fixedNegitiable.toString()?? 'NA',
            productResponse.categories[i].color.toString()?? 'NA',
            productResponse.categories[i].type.toString()?? 'NA',
            productResponse.categories[i].material.toString()?? 'NA',
            productResponse.categories[i].newOld.toString()?? 'NA',
            productResponse.categories[i].modelName.toString()?? 'NA',
            productResponse.categories[i].brandName.toString()?? 'NA',
            productResponse.categories[i].brand.toString()?? 'NA',
            productResponse.categories[i].description.toString()?? 'NA',
            productResponse.categories[i].price.toString()?? 'NA',
            productResponse.categories[i].negotiable.toString()?? 'NA',
            productResponse.categories[i].city.toString()?? 'NA',
            productResponse.categories[i].country.toString()?? 'NA',
            productResponse.categories[i].state.toString()?? 'NA',
            productResponse.categories[i].location.toString()?? 'NA',
            productResponse.categories[i].lang.toString()?? 'NA',
            productResponse.categories[i].lat.toString()?? 'NA',
            productResponse.categories[i].postalcode.toString()?? 'NA',
            productResponse.categories[i].tags.toString()?? 'NA',
            productResponse.categories[i].img1.toString()?? 'NA',
            productResponse.categories[i].img2.toString()?? 'NA',
            productResponse.categories[i].img3.toString()?? 'NA',
            productResponse.categories[i].img4.toString()?? 'NA',
            productResponse.categories[i].boxAndPaperImage.toString()?? 'NA',
            productResponse.categories[i].isOriginalRceipt.toString()?? 'NA',
            productResponse.categories[i].seller.toString()?? 'NA',
            productResponse.categories[i].package.toString()?? 'NA',
            productResponse.categories[i].adminView.toString()?? 'NA',
            productResponse.categories[i].howmuchtimeremaining.toString()?? 'NA',
            productResponse.categories[i].fixedNegitiable.toString()?? 'NA',
            productResponse.categories[i].isFeatured.toString()?? 'NA',
            productResponse.categories[i].isStatus.toString()?? 'NA',
            productResponse.categories[i].deleteRequest.toString()?? 'NA',
            productResponse.categories[i].updatedDate.toString()?? 'NA',
            productResponse.categories[i].createdDate.toString()?? 'NA',
            productResponse.categories[i].expiryDate.toString()?? 'NA',
          )
      );
    }

    print('_productsOfCategoryList:'+_productsOfCategoryList[0].toString());
  }

  Future<void> getCategoryList() async {
    try {
      Options options = Options(
        contentType: ContentType.parse('application/json'),
      );

      var response = await dio.get('category.php', options: options);

      if (response.statusCode == 200 || response.statusCode == 201) {
        var responseJson = json.decode(response.data);

        print("Response: "+responseJson.toString());

        return responseJson;
      } else
        print("Error: "+response.statusCode.toString());
    } on DioError catch (exception) {
      if (exception == null ||
          exception.toString().contains('SocketException')) {
        print("Error: "+"Network Error");
      } else if (exception.type == DioErrorType.RECEIVE_TIMEOUT ||
          exception.type == DioErrorType.CONNECT_TIMEOUT) {
        print("Error: "+ "Could'nt connect, please ensure you have a stable network.");
      } else {
        return null;
      }
    }
  }

}
